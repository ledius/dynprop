# -*- coding: utf-8 -*-
#!/usr/bin/env python3

from distutils.core import setup, Extension

setup(name='pyDynprop',
      version = '1.0',
      description = 'Analyze dynamical and statistical properties of molecular dynamic trajectories',
      author = 'Matti Ropo',
      author_email = 'matti.ropo@gmail.com',
      maintainer = 'Matti Ropo',
      maintainer_email = 'matti.ropo@gmail.com',
      packages = ['dynprop'],
      ext_modules = [Extension('dynprop.analyse',['src/analyse.c'],
                               extra_compile_args = ['-fopenmp'],
                               extra_link_args = ['-lgomp'])],
      classifiers = [
          'Development Status :: 5 - Production/Stable',
          'License :: OSI Approved :: MIT License',
          'Topic :: Scientific/Engineering :: Physics',
          'Intended Audience :: Science/Research',
          'Programming Language :: Python',
          'Programming Language :: C',
          'Environment :: Console' ],
      )
      


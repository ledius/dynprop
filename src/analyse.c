#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include <omp.h>
#include <complex.h>
#include <stdio.h>
#include <stdlib.h> // For exit function


double arngt(double *coord,double *lattm, int ri, int ai, int noa, int tr, int ta)
{
  /* 
   * Calculate distance between two atoms. Takes account perdiodic 
   * boundary conditions
   *
   * Arguments:
   * coord : array containing coordinates
   * lattm : array for lattice matric
   *    ri : atom r index
   *    ai : atom a index
   *   noa : number of atoms
   *   tr  : time index for ri
   *   ta  : time index for ai 
   *
   * Return:
   * double : range between atoms
   */
  
  int i,j,k,l; //Counters

  double rng = 100.0;
  double nv; //Shift with lattice vector
  double ds;
  double dd[3];
  double d;
  
  // Get distance vector
  for(l=0;l<3;l++) {
    dd[l] = coord[tr*noa*6+ri*6+l]-coord[ta*noa*6+ai*6+l];
  }
  //
  for(i=-1;i<2;i++) {
    for(j=-1;j<2;j++) {
      for(k=-1;k<2;k++){
	ds = 0.0;
	for(l=0;l<3;l++) // Go through coordinates 1,2 and 3
	  {
	    // Shift from the lattice vectors
	    nv=(double)i*lattm[3*l]+(double)j*lattm[3*l+1]+(double)k*lattm[3*l+2];
       	    // Get new shifted distance vector
	    d = dd[l]+nv;
	    ds+=d*d;
	  }
	// Check if new distance is smaller than previous result
	ds = sqrt(ds);
	if(ds < rng) rng = ds;             
      }
    }
  }
  return rng;
}

double perrng(double *coord,double *lattm,int ri, int ai,int noa,int t)
{
  /* Calculate range in periodic supercell between two atoms,
   * but in this version differ from arng function
   * 
   * This is used when percolation is calculated.
   *
   *
   * Arguments:
   * coord : array for coordinates
   * lattm : array for lattice vectors
   * ri    : index for atom r
   * ai    : index for atom a
   * noa   : number of atoms in system
   * t     : time index 
   *
   * Return
   * double : range between atoms.
   *
   */
    int i,j,k,l; //Counters

    double rng = 100.0;
    double nv;
    double ds;
    double d;
    double dd[3];

    // Get distance vector
    for(l=0;l<3;l++){
        dd[l]=coord[t*noa*6+ri*6+l]-coord[t*noa*6+ai*6+l];
    }
    for(i=0;i<2;i++) { // Difference to arng function is in index of these loops.
        for(j=0;j<2;j++) {
            for(k=0;k<2;k++){
                ds = 0.0;
                for(l=0;l<3;l++) // Go through coordinates 1,2 and 3
                {
                    nv=(double)i*lattm[3*l]+(double)j*lattm[3*l+1]+(double)k*lattm[3*l+2];
                    d = dd[l]+nv;
                    ds+=d*d;
                }
                ds = sqrt(ds);
                if(ds < rng) rng = ds;             
            }
        }
    }
    return rng;
}

double * avecres(double *coord,double *lattm,int ri, int ai,int noa,int t)
{
  /* Find range vector between atom ri and ai.
   * Take account perdiodic boundary conditions
   * 
   * Arguments:
   * coord  : array containing coordinates
   * lattm  : array for lattice vectors
   * ri     : index for atom r
   * ai     : index for atom i
   * noa    : number of atoms in system
   * t      : time index of frame
   *
   * Return:
   * double[3] : results vector
   */
    int i,j,k,l; //Counters

    double rng = 100.0;
    double nv;
    double ds;
    double dd[3];
    double nvec[3];
    static double svec[3];

    // Distance vector
    for(l=0;l<3;l++){
        dd[l]=coord[t*noa*6+ri*6+l]-coord[t*noa*6+ai*6+l];
    }
    for(i=-1;i<2;i++) {
        for(j=-1;j<2;j++) {
            for(k=-1;k<2;k++){
                ds = 0.0;
                for(l=0;l<3;l++) // Go through coordinates 1,2 and 3
                {
                    nv=(double)i*lattm[3*l]+(double)j*lattm[3*l+1]+(double)k*lattm[3*l+2];
                    nvec[l] = dd[l]+nv;
                    ds+=nvec[l]*nvec[l];
                }
                ds = sqrt(ds);
                if(ds < rng) 
		{
		  rng = ds;             
		  for(l=0;l<3;l++)
		  {
		    svec[l]=nvec[l];
		  }
		}
            }
        }
    }
    return svec;
}


double dotp(double *q,double *r) // Calculate dot product
{
  /*
   * Calculate dot product of vectors q and r
   *
   * Argumetns:
   * q : First vector
   * r : Second vector
   *
   * Return:
   *  double: dot product
   */
    double result = 0.0;
    int i;
    for(i=0;i<3;i++){
        result+=q[i]*r[i];
    }
    return result;
}



double crossp(double *q,double *r) // calculate cross product 
{
  /*
   *  Calculates cross product of vectro q and r
   *
   * Arguments:
   *  q : First vector
   *  r : Second vector
   *
   * Return:
   *  double : cross product
   */
    double cross[3];
    cross[0] = (q[1]*r[2]-q[2]*r[1]);
    cross[1] = (q[2]*r[0]-q[0]*r[2]);
    cross[2] = (q[0]*r[1]-q[1]*r[0]);

    return cross[0];
}

void avec(double *coord,double *lattm,double *shortest,int ri, int ai,int noa,int t)
{
  /* Find range vector between atom ri and ai.
   * Take account perdiodic boundary conditions
   * 
   * Arguments:
   * coord    : coordinate matrix
   * lattm    : Matrix for lattice vectors
   * shortest : results vector
   * ri       : index for first atom
   * ai       : index for second atom
   * noa      : Number of atoms in the system
   * t        : Time steps in which vector is calculated
   */

    int i,j,k,l; //Counters

    //double btoa =  0.52917720859; //Bohrs to Angstroms

    double rng = 100.0;
    double nv;
    double ds;
    double dd[3];
    double nvec[3]; //temporary vector

    // Distance vector
    for(l=0;l<3;l++){
        dd[l]=coord[t*noa*6+ri*6+l]-coord[t*noa*6+ai*6+l];
    }

    
    for(i=-1;i<2;i++) {
        for(j=-1;j<2;j++) {
            for(k=-1;k<2;k++){
                ds = 0.0;
                for(l=0;l<3;l++){
		  nv=(double)i*lattm[3*l]+(double)j*lattm[3*l+1]+(double)k*lattm[3*l+2];
		  nvec[l] = dd[l]+nv;
		  ds+=nvec[l]*nvec[l];
                }
                ds = sqrt(ds);
                if(ds < rng){
		  rng = ds;
		  for(l=0;l<3;l++){ // Copy nvec to shortest to save shortest vector
		    shortest[l] = nvec[l];
		  }
                }             
            }
        }
    }
}



PyObject* bondangles_c(PyObject* self, PyObject *args)
{
  /* 
   * Calculate bond angles between three atoms within the cutoff distance from each other.
   *
   * Arguments:
   *
   * py_coord  : Matrix for coordinates
   * py_latt   : Matrix for lattice vectors
   * py_rm     : Range matrix
   * py_angles : results angles
   * t         : timestep for calculation
   * noa       : Number of atoms in system
   * cutoff    : cutoff for neighbors 
   */
  
    PyArrayObject *py_coord; // Array of the timesteps
    PyArrayObject *py_latt; //lattice vectors
    PyArrayObject *py_rm;  //Rangematrix
    PyObject *py_angles; // angles list 
    int t; // timestep for calculation
    int noa; // number of atoms in system
    double cutoff; // Cutoff of neighbors

    if (!PyArg_ParseTuple(args,"OOOOdii",&py_coord,&py_latt,&py_rm,&py_angles,&cutoff,&noa,&t))
        return NULL;

    double *coord = PyArray_DATA(py_coord);
    double *latt = PyArray_DATA(py_latt);
    double *rm = PyArray_DATA(py_rm);


    int MAXNN = 100; // This is not good as now we have hard coded maximum number of neighbours
    int i,j,l,nj,nk; //Counters
    int ngbrs[MAXNN]; 
    int nn;
    double norm_pij,norm_pik;
    double d,dotpp,angle;
    double pij[3],pik[3]; 

    for(i=0;i<noa;i++)
    {
        for(j=0;j<MAXNN;j++) // Reset ngbrs list
        {
            ngbrs[j]=-1;
        }

	// Find neighbors for atom i
        nn = 0;
        for(j=0;j<noa;j++)
        {
            if(i==j) continue;
            if(rm[i+noa*j] < cutoff)
            {
                ngbrs[nn]=j;
                nn+=1;
            }
        } // neighbours list ready

        for(nj=0;nj<nn;nj++) // Go trought all neighbours
        {
            if(ngbrs[nj] == i || ngbrs[nj] == -1) continue;
            avec(coord,latt,pij,i,ngbrs[nj],noa,t); // find vector between atom i and its neighbor
            d = 0.0;
            for(l=0;l<3;l++) // Calculate norm of Pij
            {
                d+=pij[l]*pij[l];
            }
            norm_pij = sqrt(d);
            for(nk=0;nk<nn;nk++)
            {
                if (ngbrs[nk]==i || ngbrs[nk]==ngbrs[nj] || ngbrs[nk]==-1) continue;
                avec(coord,latt,pik,i,ngbrs[nk],noa,t);
                d = 0.0;
                for(l=0;l<3;l++) // Calculate norm of pik
                {
                    d+=pik[l]*pik[l];
                }
                norm_pik = sqrt(d);
            
                // calculate dot product
                dotpp = 0.0;
                for(l=0;l<3;l++)
                {
                    dotpp+=pik[l]*pij[l];
                }
                angle = acos(dotpp/(norm_pij*norm_pik));
                angle = angle*180.0/M_PI;
                //printf("%i nn: %i %i angle: %f\n",i,ngbrs[nj],ngbrs[nk],angle);
                PyList_Append(py_angles,PyFloat_FromDouble(angle));
            }
            

        }
    }
  Py_RETURN_NONE;
}

PyObject* rngmatrix_c(PyObject *self,PyObject *args)
{
  /*
   * Calculate range matrix between atoms in a time step 
   *
   * Arguments:
   * py_coord : array for coordinates
   * py_latt  : Array for lattice vectors
   * py_rm    : Array for results rangematrix 
   * t        : timestep for the rangematrix
   * noa      : Number of atoms in the system
   */
  
  PyArrayObject *py_coord; // Array of the timesteps
  PyArrayObject *py_latt;  //Lattice vectors 
  PyArrayObject *py_rm; // Results rangematrix
  int t; // Time for the calculation of rangematrix
  int noa; //Number of atoms in the system

  if (!PyArg_ParseTuple(args,"OOOii",&py_coord,&py_latt,&py_rm,&noa,&t))
    return NULL;

  double *coord = PyArray_DATA(py_coord);
  double *latt  = PyArray_DATA(py_latt);
  double *rm = PyArray_DATA(py_rm);

  int i,j; // Counters
  double rng; 

  for(i=0;i<noa;i++)
  {
    for(j=i+1;j<noa;j++)
    {
      rng = arngt(coord,latt,i,j,noa,t,t); // get range between atoms i and j
      rm[i*noa+j]=rng;
      rm[j*noa+i]=rng;
    }
  }
  Py_RETURN_NONE;
}

PyObject* rmperc_c(PyObject *self,PyObject *args)
{
  /*
   * Calculate range matrix for percolation test.
   * 
   * The difference between this and normal rangematrix is
   * the way how periodic boundary conditions are taken account
   *
   * Arguments:
   * py_coord : array for coordinates
   * py_latt  : Array for lattice vectors
   * py_rm    : Array for results rangematrix 
   * t        : timestep for the rangematrix
   * noa      : Number of atoms in the system 
   *
   */
  PyArrayObject *py_coord; // Array of the timesteps
  PyArrayObject *py_latt;  //Lattice vectors 
  PyArrayObject *py_rm; // Results rangematrix
  int t; // Time for the calculation of rangematrix
  int noa; //Number of atoms in the system

  if (!PyArg_ParseTuple(args,"OOOii",&py_coord,&py_latt,&py_rm,&noa,&t))
    return NULL;

  double *coord = PyArray_DATA(py_coord);
  double *latt  = PyArray_DATA(py_latt);
  double *rm = PyArray_DATA(py_rm);

  int i,j; // Counters
  double rng; 

  for(i=0;i<noa;i++)
  {
    for(j=i+1;j<noa;j++)
    {
        rng = perrng(coord,latt,i,j,noa,t);
        rm[i*noa+j]=rng;
        rm[j*noa+i]=rng;
    }
  }
  Py_RETURN_NONE;
}


PyObject* movetooriginal_c(PyObject *self,PyObject *args)
/*
*  Move atomic coordinates to the original supercell
*  Need transformation matrix and inversematrix for it.
*
* Arguments:
* py_coord : array for the atomic coordinates and velocities
* py_tmatrix : Transformation matrix conctructed from lattice vectors
* py_invmatx : Inverse transformation matrix.
*/
{


  PyArrayObject* py_coord; // array of the atomic coordinates
  PyArrayObject* py_tmatrix; // Transformation matrix
  PyArrayObject* py_invmat; // Inverse matrix

  int noa,nos; //Number of atosm and steps
  //  double btoa =  0.52917720859; //Bohrs to Angstroms

  if (!PyArg_ParseTuple(args,"OOOii",&py_coord,&py_tmatrix,&py_invmat,&noa,&nos))
    return NULL;

  int t,atom; //Time and atom counters
  int i,j; // Coordinate counter



  double *coord = PyArray_DATA(py_coord);
  double *tmatrix = PyArray_DATA(py_tmatrix);
  double *invmat = PyArray_DATA(py_invmat);

  double vec[3]; // Coordinate vector in angstroms
  double b_vec[3]; // Vector in local coordinates

  for(t=0; t<nos;t++) // Go over all timesteps
  {
    for(atom=0;atom<noa;atom++) // Go over all atoms
    {   
      for(j=0;j<3;j++)
        {
	  b_vec[j] = 0.0; // Reset b_vec
	  // Transform to local coordinates
	  for(i=0;i<3;i++)
            {
	      //b_vec[j] += invmat[i*3+j]*coord[t*noa*6+atom*6+i];
	      b_vec[j] += invmat[j*3+i]*coord[t*noa*6+atom*6+i];
            };
	  // Move coordinates to original supercell
	  b_vec[j]=fmod(b_vec[j],1.0);
	  if (b_vec[j]< 0.0)
            {
	      b_vec[j]=1.0+b_vec[j];
            }
        };

      for(j=0;j<3;j++)
        {
	  vec[j] = 0.0; //Reset vector
	  // Transform back to original coordinates
	  for(i=0;i<3;i++)
            {
	      vec[j] += tmatrix[j*3+i]*b_vec[i];
	      //continue;
            };
	  // vec[j]= b_vec[j]; //Debug
	  // Replace original coordinates with new ones
	  coord[t*noa*6+atom*6+j]=vec[j];      
        };
    };
  };
  Py_RETURN_NONE;
};


PyObject* ecn_frame_c(PyObject *self, PyObject *args)
/* Calculate effective coordination number for one frame
 * Return list of ecn for each atoms
 *
 * Arguments:
 *
 * py_rmatrix   : rangematrix
 * py_ecni      : Array of ecn for each atoms
 * noa          : Number of atoms
 */
{

  PyArrayObject* py_rmatrix; // range matrix
  PyArrayObject* py_ecni; // ecn array for each atom 
  int noa; //number of atoms

  if (!PyArg_ParseTuple(args,"OOi",&py_rmatrix,&py_ecni,&noa))
    return NULL;

  double *rm = PyArray_DATA(py_rmatrix);
  double *ecni = PyArray_DATA(py_ecni);  

  //  n = py_rmatrix-> dimensions[0]; // number of rows
  //m =py_rmatrix-> dimensions[1]; // number of columns
  //int size = PyArray_SIZE(py_rmatrix); // Total number of matrix

  int i,j;
  int davready;
  double dav,newdav,texp,sumexp,sumdexp;

  for(i=0;i<noa;i++) // Go over all atoms
    {
      davready = 0;
      // find initial dav 
      dav = 20.0;
      for(j=0;j<noa;j++)
	{
	  if (i != j)
	    {
	      if (rm[i*noa+j]< dav) { dav = rm[noa*i+j];};
	    };
	};
      // Start calculating dav
      while (!davready)
	{
	  sumexp = 0.0;
	  sumdexp = 0.0;
	  for(j=0;j<noa;j++) // Calculate sumexp and sumdexp
	    {
	      if (i != j) // Sum go over all other atoms 
		{
		  texp = exp(1.0-pow(rm[j+i*noa]/dav,6.0));
		  sumdexp += rm[j+i*noa]*texp;
		  sumexp += texp;
		};
	    }; // Sum exp and sumdexp calculated 
	  newdav = sumdexp/sumexp; // calculate new dav
	  if ( fabs(dav-newdav) < 0.00001)
	    {
	      davready = 1;
	    };
	  dav = newdav;
	}; // Dav ready
      //printf("C : %i %f\n",i, dav);
      for(j=0;j<noa;j++) // Calculate /*ecn*/i
	{
	  if(i != j ) {
	    ecni[i] += exp(1.0-pow(rm[j+i*noa]/dav,6.0));
	  };
	};
    };

  Py_RETURN_NONE;
};

PyObject* vv_autocorr_atom_c(PyObject *self, PyObject *args)
{
  /* 
   * Calculate velocity-velocity autocorrelation for trajetory
   *
   * Arguments:
   * py_vel     : array for velocities
   * py_results : results, autocorrelation function array
   * noa        : Number of atoms in system 
   * nos        : Number of steps in system
   * maxstep    : maximum step
   * wsize      : size of correlation window in framse
   * atom       : index for atom 
   */
  PyArrayObject* py_vel; //Array of the velocities
  PyArrayObject* py_result; // Result array
  int noa,nos,maxstep; //Number of atoms,Number of steps, maximum step
  int wsize; // window size
  int atom; // index of atom for calculation

  if (!PyArg_ParseTuple(args,"OOiiii",&py_vel,&py_result,&atom,&noa,&maxstep,&wsize,&nos))
    return NULL;

  int t,d; // Counters
  int i; // position counters

  double *vel = PyArray_DATA(py_vel);
  double *results = PyArray_DATA(py_result);
  double C;
  double dot,norm;

  for(d=0; d<maxstep;d++)
    {
      //      printf("eka d: %i maxstep: %i atom: %i\n",d,maxstep,atom);
      C=0.0;
      for(t=0;t<d+wsize;t++)
	{
	  norm = 0.0;
	  dot = 0.0;
	  for(i=3;i<6;i++) //Calculate dot products
	    {
	      norm += vel[t*noa*6+atom*6+i]*vel[t*noa*6+atom*6+i];
	      dot += vel[t*noa*6+atom*6+i]*vel[(t+d)*noa*6+atom*6+i];
	    };
	    C += dot/norm;
	};
      C = C/(float)(nos-d); //Normalization
      results[d] = C;
    };
  Py_RETURN_NONE;
};


PyObject* pairc_c(PyObject *self, PyObject *args)
{
  /*
   * Calculates pair correlation function
   *
   * Arguments:
   * py_coord  : array of coordinates
   * py_gr     : array to save the pair correlation function 
   * py_latt   : lattice matrix
   * nbins     : Number of bins
   * maxrng    : maximum range to take account
   * dr        : delta r for bins
   * maxsteps  : Maximum step which is takeng account
   * noa       : number of atoms
   */
  PyArrayObject* py_coord; // array of the atomic coordinates
  PyArrayObject* py_gr; // VanHove function ie. results
  PyArrayObject* py_latt; // Lattice matrix
  int nbins; // number of bins
  double maxrng;
  double dr;
  int maxsteps; //number of steps
  int noa; // number of atoms

  if (!PyArg_ParseTuple(args,"OOOiddii",&py_coord,&py_gr,&py_latt,&nbins,&maxrng, &dr,&noa, &maxsteps))
    return NULL;

  double *data = PyArray_DATA(py_coord);
  double *gr = PyArray_DATA(py_gr);
  double *latt = PyArray_DATA(py_latt);
  int i,j,t,ll;
  double rng;

  for(t=0;t<maxsteps;t++){
    printf("Time step: %i of %i\n",t+1,maxsteps);
    for(i=0;i<noa-1;i++) {
       for(j=i+1;j<noa;j++){
	 rng = arngt(data,latt,i,j,noa,t,t);
            if(rng < maxrng){
                ll = (int) (rng/dr);		    
                gr[ll]+=2; // We have two pairs to count from i-> j and j -> i
            }    
        }
    }
  }
  Py_RETURN_NONE;
};


PyObject* vanHove(PyObject *self, PyObject *args)
{
  /*
   * Calculates vanHove correlation function
   *
   * Arguments:
   * py_coord : Array for coordinates
   * py_grt   : Array to save vanHove function, result
   * py_grtS  : Array to save self part of vanHove function, result
   * py_latt  : Array for lattice matrix
   * nbins    : number of bins
   * maxrng   : maximum range to take account
   * dr       : width of bins
   * steps    : number of steps
   * noa      : Number of atoms in system
   * wstep    : Size of window step
   * wsize    : Size of correlation window
   */
  PyArrayObject* py_coord; // array of the atomic coordinates
  PyArrayObject* py_gtr; // VanHove function ie. results
  PyArrayObject* py_gtrS; // Self part of VanHove function
  PyArrayObject* py_latt; // Lattice matrix
  int nbins; // number of bins
  double maxrng;
  double dr;
  int steps; //number of steps
  int noa; // number of atoms
  int wstep; // How big is windows step
  int wsize; // How big is the window

  if (!PyArg_ParseTuple(args,"OOOOiddiiii",&py_coord,&py_gtr,&py_gtrS,&py_latt,&nbins,&maxrng, &dr,&noa, &steps,&wstep,&wsize))
    return NULL;

  double *data = PyArray_DATA(py_coord);
  double *gtr = PyArray_DATA(py_gtr);
  double *gtrS = PyArray_DATA(py_gtrS);
  double *latt = PyArray_DATA(py_latt);

  int maxsteps,window;
  int i,j,t,dt,ll;
  double rng;
  maxsteps = steps; // maximum step analyzed from trajectory should be number of step/2
  window = wsize; // Size of the analysis window
  
  for(t=0;t<maxsteps;t=t+wstep){
    printf("Time step: %i of %i\n",t+1,maxsteps);
    // here to be parallelized by openMP
    #pragma omp parallel for private (dt,i,j,rng,ll)
    for(dt=t;dt<t+window;dt++) { // This for testing purposes
    //for(dt=t;dt<t+2;dt++) { // This for testing purposes
       for(i=0;i<noa;i++) {
           for(j=0;j<noa;j++){
                rng = arngt(data,latt,i,j,noa,t,dt);
                if(rng < maxrng){
                    ll = (int) (rng/dr);
        		    if (j == i){
                        #pragma omp atomic
                        gtrS[(dt-t)*nbins+ll]+=1;  
                    }
                    else {
                    #pragma omp atomic // make sure that each processors write nicely
                        gtr[(dt-t)*nbins+ll]+=1;  
                    }
                }
           }
        }
      }
  }

  Py_RETURN_NONE;
};

PyObject* vanHove_onlyself(PyObject *self, PyObject *args)
{
  /*
   * Calculate only self vanHove correlation function
   *
   * Arguments:
   * py_coord : Array for coordinates
   * py_grt   : Array to save vanHove function. This isn't used!
   * py_grtS  : Array to save self part of vanHove function, result
   * py_latt  : Array for lattice matrix
   * nbins    : number of bins
   * maxrng   : maximum range to take account
   * dr       : width of bins
   * steps    : number of steps
   * noa      : Number of atoms in system
   * wstep    : Size of window step
   * wsize    : Size of correlation window
   *
   */
  PyArrayObject* py_coord; // array of the atomic coordinates
  PyArrayObject* py_gtr; // VanHove function ie. results
  PyArrayObject* py_gtrS; // Self part of VanHove function
  PyArrayObject* py_latt; // Lattice matrix
  int nbins; // number of bins
  double maxrng;
  double dr;
  int steps; //number of steps
  int noa; // number of atoms
  int wstep; // How big is windows step

  if (!PyArg_ParseTuple(args,"OOOOiddiii",&py_coord,&py_gtr,&py_gtrS,&py_latt,&nbins, &maxrng, &dr,&noa, &steps,&wstep))
    return NULL;

  double *data = PyArray_DATA(py_coord);
  //double *gtr = PyArray_DATA(py_gtr);
  double *gtrS = PyArray_DATA(py_gtrS);
  double *latt = PyArray_DATA(py_latt);
  //double *rcoord[3];
  int maxsteps,window;
  int i,t,dt,ll;
  double rng;
  maxsteps = steps; // maximum step analyzed from trajectory should be number of step/2
  window = steps; // size of the analyze window in time 

  for(t=0;t<maxsteps;t=t+wstep){
    printf("Time step: %i of %i\n",t+1,maxsteps);
    // here to be parallelized by openMP
    #pragma omp parallel for private (dt,i,rng,ll)
    for(dt=t;dt<t+window;dt++) { // This for testing purposes
       for(i=0;i<noa;i++) {
            rng = arngt(data,latt,i,i,noa,t,dt);
            if(rng < maxrng){
                //if (rng < 0.3) printf("Alle 1 %f i=%i j =%i, t=%i, dt=%i\n",rng,i,j,t,dt);
                ll = (int) (rng/dr);
                gtrS[(dt-t)*nbins+ll]+=1;  
                }
           }
        }
      }
  Py_RETURN_NONE;
  }


PyObject* microdens(PyObject *self, PyObject *args)
{
  /*
   * Calculates microscopic density
   *
   * Arguments:
   * py_coord  : Array for the atomic coordinates
   * py_rho    : Array to save intermediate scattering function, result
   * py_qvecs  : Array of q vectors used in the calculation
   * maxsteps  : number of steps
   * noa       : Number of atoms in system
   * noq       : Number of q vetors 
   *
   */
  PyArrayObject* py_coord; // array of the atomic coordinates
  PyArrayObject* py_rho; // Intermediate Scattering function ie. results
  PyArrayObject* py_qvecs; // Lattice matrix
  int maxsteps; //number of steps
  int noa; // number of atoms
  int noq; // number of q vectors

  if (!PyArg_ParseTuple(args,"OOOiii",&py_coord,&py_rho,&py_qvecs,&noq,&noa, &maxsteps))
    return NULL;

  double *data = PyArray_DATA(py_coord);
  complex *rho = PyArray_DATA(py_rho);
  double *qvecs = PyArray_DATA(py_qvecs);

  int i,q,t;
  double norm;
  complex qr;
  complex ii = 1.0*I; 

  norm = sqrt((double) noa);

  //OMP parallelization
  #pragma omp parallel for private (t,q,qr,i)
  for(t=0;t<maxsteps;t++){
    printf("Microscopi density step: %i / %i\n",t,maxsteps);
    for(q=0;q<noq;q++) {
        qr = 0.0 +0.0*I;
        for(i=0;i<noa;i++) {
            // calculate exp(i* q*r)
            qr+= cexp(ii*dotp(&qvecs[q*3],&data[t*noa*6+i*6]));
            }    
        rho[t*noq+q] = qr/norm; 
        }
    }
  Py_RETURN_NONE;
}

PyObject* self_isf(PyObject *self, PyObject *args)
{
  /*
   * Calculate self intermediate scatering function
   *
   * Arguments:
   * py_coord  : Array for the atomic coordinates
   * py_rho    : Array to save intermediate scattering function, result
   * py_qvecs  : Array of q vectors used in the calculation
   * maxsteps  : number of steps
   * noa       : Number of atoms in system
   * noq       : Number of q vetors 
   *
   */

  PyArrayObject* py_coord; // array of the atomic coordinates
  PyArrayObject* py_rho; // Intermediate Scattering function ie. results
  PyArrayObject* py_qvecs; // Lattice matrix
  int maxsteps; //number of steps
  int noa; // number of atoms
  int noq; // number of q vectors

  if (!PyArg_ParseTuple(args,"OOOiii",&py_coord,&py_rho,&py_qvecs,&noq,&noa, &maxsteps))
    return NULL;

  double *data = PyArray_DATA(py_coord);
  complex *rho = PyArray_DATA(py_rho);
  double *qvecs = PyArray_DATA(py_qvecs);

  //double *rcoord[3];
  int i,q,t;
  //double norm;
  complex qr;
  complex ii = 1.0*I; 

  //norm = sqrt((double) noa);

  for (i=0;i<noa;i++) {
    printf("self ISF for atom: %i / %i\n",i+1,noa);	
    for(q=0;q<noq;q++) {
      //qr0 = cexp(ii*dotp(&qvecs[q*3],&data[0*noa*6+i*6]));
      qr = 0.0+0.0*I;
      for(t=1;t<maxsteps;t++){
	// calculate exp(i* q*r)
	qr += cexp(ii*dotp(&qvecs[q*3],&data[t*noa*6+i*6]));	
      }    
      rho[t*noq+q] = qr;
        }
    }
  Py_RETURN_NONE;
}

PyObject* particlecurrents(PyObject *self, PyObject *args)
{
  /*
   * Calculates longitudal and transverse particle currents
   * for given q-vectors
   *
   * Arguments:
   *  py_coord : Array of the atomic coordinates
   *  py_jl    : Array for longitudinal partice current
   *  py_jt    : Array for transverse particle current
   *  py_qvecs : Array of q vectors used in the calculation
   * maxsteps  : Number of steps
   * noa       : Number of atoms
   * noq       : Number of q vectors
   *
   */
  PyArrayObject* py_coord; // array of the atomic coordinates
  PyArrayObject* py_jl; // longitude current
  PyArrayObject* py_jt; // transverse current
  PyArrayObject* py_qvecs; // q vectors
  int maxsteps; //number of steps
  int noa; // number of atoms
  int noq; // number of q vectors

  if (!PyArg_ParseTuple(args,"OOOOiii",&py_coord,&py_jl,&py_jt,&py_qvecs,&noq,&noa, &maxsteps))
    return NULL;

  double *data = PyArray_DATA(py_coord);
  complex *jl = PyArray_DATA(py_jl); //Longitudal part
  complex *jt = PyArray_DATA(py_jt); //Transverse part
  double *qvecs = PyArray_DATA(py_qvecs);

  int i,q,t;
  double norm;
  complex eqr,jql;
  complex ii = 1.0*I; 

  complex jqt[3];

  //double atu2fs = 0.02418884326505;
  norm = sqrt((double) noa);


  #pragma omp parallel for private (t,q,eqr,i,jql,jqt)
  for(t=0;t<maxsteps;t++){
    printf("Particle current (vector) step: %i / %i\n",t,maxsteps);
    // Debug

    for(q=0;q<noq;q++) {
     jql = 0.0 +0.0*I;
     jqt[0] = 0.0 +0.0*I;
     jqt[1] = 0.0 +0.0*I;
     jqt[2] = 0.0 +0.0*I;
     //jqt[0] = jqt[1] = jqt[2]= 0.0 +0.0*I;   
	//qnorm = sqrt(dotp(&qvecs[q*3],&qvecs[q*3]));
	for(i=0;i<noa;i++) {
	  // calculate exp(i*q.r)
	  eqr = cexp(ii*dotp(&qvecs[q*3],&data[t*noa*6+i*6]));
	  //longitudal part
	  jql += dotp(&qvecs[q*3],&data[t*noa*6+i*6+3])*eqr; // Without qnorm
	  // transverse part
	  jqt[0] += (qvecs[q*3+1]*data[t*noa*6+i*6+3+2]-qvecs[q*3+2]*data[t*noa*6+i*6+3+1])*eqr;
	  jqt[1] += (qvecs[q*3+2]*data[t*noa*6+i*6+3+0]-qvecs[q*3+0]*data[t*noa*6+i*6+3+2])*eqr;
	  jqt[2] += (qvecs[q*3+0]*data[t*noa*6+i*6+3+1]-qvecs[q*3+1]*data[t*noa*6+i*6+3+0])*eqr;
	}    
     jl[t*noq+q] = jql/norm; // Normalize with sqrt(number of atoms)    
     jt[t*noq*3+q*3+0] = jqt[0]/norm; // Normalize with sqrt(number of atoms)
     jt[t*noq*3+q*3+1] = jqt[1]/norm; // Normalize with sqrt(number of atoms)
     jt[t*noq*3+q*3+2] = jqt[2]/norm; // Normalize with sqrt(number of atoms)
    
    }
   }
  Py_RETURN_NONE;
}



PyObject* correlation_vec(PyObject *self, PyObject *args)
{
  /*
   * Calculates correlation function for complex vector
   *
   * Arguments:
   *  py_x      : Array to calculate the correlation from
   *  py_corr   : Array for correlation functio, ie result
   *  py_qset_i : Array of indeces for eqch q value (shell)
   *  py_qn     : Vector containun number of q vector in each shell
   *  wsize     : Size of the correlation window
   *  laststep  : Last step to start correlation window
   *
   */
  PyArrayObject* py_x; // array to calculte the correlation from
  PyArrayObject* py_corr; // Correlation function ie. results  maxstep * len(qset)
  PyArrayObject* py_qset_i; // array of  indeces for each q value (shell)
  PyArrayObject* py_qn; // vector containing number of q vectors in each shell

  int laststep;
  int wsize;
  
  if (!PyArg_ParseTuple(args,"OOOOii",&py_x,&py_corr,&py_qset_i,&py_qn,&wsize,&laststep))
    return NULL;

  complex *X = PyArray_DATA(py_x);
  double *corr = PyArray_DATA(py_corr);
  long int *qset = PyArray_DATA(py_qset_i);
  long int *qn = PyArray_DATA(py_qn);


  int finalstep = PyArray_SHAPE(py_x)[0];
  //int finalstep = py_x->dimensions[0];
  int maxsteps = laststep;
  //int maxsteps = py_corr->dimensions[0];
  int nqset = PyArray_SHAPE(py_corr)[1]; //Number of qsets
  //int nqset = py_corr->dimensions[1]; // NUmber of qsets 
  int maxqi = PyArray_SHAPE(py_qset_i)[1]; //Maximum number of vectors in any q shell
  //int maxqi = py_qset_i->dimensions[1]; //maximum number of vectors in any q shell
  int nq = PyArray_SHAPE(py_x)[1];
  //int nq = py_x->dimensions[1];

  int window = wsize;
  double cr;
    int t,r,i;
  int q,w;

  //Sanity check
  if (maxsteps+window>finalstep) {
    printf("Maxsteps and wsize incompatible to currents!\n");
    printf("laststep: %d wsize: %d finalstep: %d\n",maxsteps, window, finalstep);
    exit(1);
  }
  
  for(r=0;r<maxsteps;r++){
    printf("Vector Correlation step: %i / %i\n",r+1,maxsteps);
    #pragma omp parallel for private (t,q,w,cr)
    for(t=r;t<r+window;t++){
       // Loop over each qset
       for(q=0;q<nqset;q++){
            //loop over each q-vectro in each shell
            cr = 0.0;
            for(w=0;w<qn[q];w++){ // Loop over vectors indeces in qset
                // Loop over vector indeces
                for(i=0;i<3;i++){
                    cr+=creal(X[t*nq*3+qset[q*maxqi+w]*3+i]*conj(X[r*nq*3+qset[q*maxqi+w]*3+i]));
                }
            } // Corrrelation summed over each q vector in shell
	    #pragma omp atomic
            corr[(t-r)*nqset+q]+=cr/ ((float) qn[q]);
        }
    }

  }
  Py_RETURN_NONE;
}

PyObject* correlation(PyObject *self, PyObject *args)
{
  /* 
   * Calculate autocorrelation of scalar value 
   *
   * Arguments:
   *  py_x      : Array to calculate the correlation from
   *  py_corr   : Array for correlation functio, ie result
   *  py_qset_i : Array of indeces for eqch q value (shell)
   *  py_qn     : Vector containun number of q vector in each shell
   *  wsize     : Size of the correlation window
   *  laststep  : Last step to start correlation window
   *
   */
  PyArrayObject* py_x; // array to calculte the correlation from
  PyArrayObject* py_corr; // Correlation function ie. results  maxstep * len(qset)
  PyArrayObject* py_qset_i; // array of  inceces for each q value (shell)
  PyArrayObject* py_qn; // vector containing number of q vectors in each shell

  int laststep;
  int wsize;
  
  if (!PyArg_ParseTuple(args,"OOOOii",&py_x,&py_corr,&py_qset_i,&py_qn,&wsize,&laststep))
    return NULL;

  complex *X = PyArray_DATA(py_x);
  double *corr = PyArray_DATA(py_corr);
  long int *qset = PyArray_DATA(py_qset_i);
  long int *qn = PyArray_DATA(py_qn);

  int finalstep = PyArray_SHAPE(py_x)[0];
  //int finalstep = py_x->dimensions[0];
  int maxsteps = laststep;
  //int maxsteps = py_corr->dimensions[0];
  int nqset = PyArray_SHAPE(py_corr)[1]; // Number of qsets
  //int nqset = py_corr->dimensions[1]; // NUmber of qsets 
  int maxqi = PyArray_SHAPE(py_qset_i)[1]; //maximum number of vectors in any q shell
  //int maxqi = py_qset_i->dimensions[1]; //maximum number of vectors in any q shell
  int nq = PyArray_SHAPE(py_x)[1];
  //int nq = py_x->dimensions[1];

  int window = wsize;
  double cr;
  int t,r;
  int q,w;

  //Sanity check
  if (maxsteps+window>finalstep) {
    printf("Maxsteps and wsize incompatible to currents!\n");
    printf("laststep: %d wsize: %d finalstep: %d\n",maxsteps, window, finalstep);
    exit(1);
  }
  
  for(r=0;r<maxsteps;r++){
    printf("Correlation step: %i / %i\n",r+1,maxsteps);
    #pragma omp parallel for private (t,q,w,cr)
    for(t=r;t<r+window;t++){
       // Loop over each qset
       for(q=0;q<nqset;q++){
            //loop over each q-vectro in each shell
            cr = 0.0;
            for(w=0;w<qn[q];w++){ // Loop over vectors indeces in qset
                cr+=creal(X[t*nq+qset[q*maxqi+w]]*conj(X[r*nq+qset[q*maxqi+w]]));
            } // Corrrelation summed over each q vector in shell
	    #pragma omp atomic
            corr[(t-r)*nqset+q]+=cr/ ((float) qn[q]);
        }
    }

  }
  Py_RETURN_NONE;
}


PyObject* Orderparam_frame(PyObject *self, PyObject *args)
{
  /*
   * Calculates orderparamter for a frame
   *
   * Arguments:
   *  py_coord  : Array of the timesteps
   *  py_latt   : Array of the lattice vectors 
   *  py_rm     : Array of the range matrix
   *  py_dotsum : array of order parameter, result
   *  frame     : Timestep to calculate order parameter
   *  nncut     : Cutoff for nearest neighbor
   *  nnncut    : Cutoff for next nearest neighbor
   *  border    : width of border 
   */
  PyArrayObject *py_coord; // Array of the timesteps
  PyArrayObject *py_latt;  //Lattice vectors 
  PyArrayObject *py_rm; // Rangematrix 
  PyArrayObject *py_dotsum; // Results dotsum
  
  int frame;
  double nncut; //Cutoff for nearest neighbours
  double nnncut; // Cutoff  for next nearest neighbours
  double border;
  
  if (!PyArg_ParseTuple(args,"OOOOiddd",&py_coord,&py_latt,&py_rm,&py_dotsum,&frame,&nncut,&nnncut, &border))
    return NULL;
 
  double *rm = PyArray_DATA(py_rm);
  double *coord = PyArray_DATA(py_coord);
  double *latt = PyArray_DATA(py_latt);
  double *dotsum = PyArray_DATA(py_dotsum);
  
  int noa = PyArray_SHAPE(py_coord)[1];
  
  int nn[20]; //Array for index of nearest neighbours	  
  //int nnn[20]; // Array for next nearest neighbours
  //int bnn[20]; //Array for nearest neighbours with border
  
  int nni;
  int count;
  
  double tvec[3]; //Temporary vector
  //double *tvec; //Temporary vector
  double tnorm;
  double dotprod;
  double sumvec[3]; //sumvector 

  double *dotvecs;
  dotvecs = (double *) malloc(sizeof(double)*noa*3); // Allocate memory for sumvectors
  
  double lnd = 11.512915464924779; // ln(d/(1-d)) calculated with 0.99999
  double A = border/(2*lnd); // Scaling factor for fermi distribution
  double c = nncut+border/2.0;
  
  double w;
  
  for(int ri=0;ri < noa;ri++)
  {
     nni = 0; //Nearest neighbour index 
     for(int i =0;i<20;i++) {
	nn[i]=-1; 
	//nnn[i] = -1;
	//bnn[i] = -1;
     }

    
    // Find nearest neighbours	
    for(int i = 0;i<noa;i++)
    {
      if (i == ri) {
	continue;
      }
      if(rm[ri+noa*i]<nncut) {
	nn[nni++]=i;
      }
      
      //else if(rm[ri+noa*i]<nnncut) {
      //nnn[nnni++]=i;
      //}
      
    } //Nearest neighbours list for atom ri is now populated
    
    for(int j=0;j<3;j++) sumvec[j]=0.0; // Initialize sum vector
    //calculate nearest  neighbour vectors
    for(int i=0;i<20;i++){
      if (nn[i] == -1) break; // End loop when first -1 is seen
      avec(coord,latt,tvec,ri,nn[i],noa,frame);
      //Calculate Norm of the tvec
      tnorm = 0.0;
      for(int j=0;j<3;j++) tnorm+=tvec[j]*tvec[j];
      tnorm = sqrt(tnorm);
      w = 1.0/(exp((rm[ri+noa*nn[i]]-c)/A)+1);
      for(int j=0;j<3;j++) sumvec[j]=sumvec[j]+w*tvec[j]/tnorm; //Add normalized and weighted

    }
    
    tnorm = 0.0;
    for(int j=0;j<3;j++) tnorm+=sumvec[j]*sumvec[j];
    tnorm = sqrt(tnorm);

    if (tnorm == 0.0) {
      for(int j=0;j<3;j++) dotvecs[ri*3+j] = 0.0;
    }
    else {
      for(int j=0;j<3;j++) dotvecs[ri*3+j] =sumvec[j]/tnorm;
    }    
  }
  // Sum vectors are ready. Next calculate the dot product between them.
  for(int ri=0;ri<noa;ri++){
    // First find nearest neighbours
    nni = 0; //Nearest neighbour index
    for(int i =0;i<20;i++) {
      nn[i]=-1;
      //nnn[i]=-1;
    }
    // Find nearest neighbours up to next nearest neighbour
    for(int i = 0;i<noa;i++) {
      if (i == ri) {
	continue;
      }
      if(rm[ri+noa*i]<nncut) {
	nn[nni++]=i;
      }
    }
    // Find 6 nearest neighbours from the 
    //for(int i=0;i<6;i++){
    //  for(int j=0;j<20;j++) {
	
    //  }
    //}

    //
    count = 0;
    for(int i=0;i<20;i++){
      if (nn[i]==-1) break;
      count +=1;
      dotprod = 0.0;
      for(int j=0;j<3;j++){
	dotprod += dotvecs[ri*3+j]*dotvecs[nn[i]*3+j];
      }
      dotsum[ri]=dotsum[ri]+fabs(dotprod);
    }
    dotsum[ri] =dotsum[ri]/ (double) count;
  }

  
  Py_RETURN_NONE;
}

PyObject* velposcor(PyObject *self, PyObject *args)
{
  /*
   * Calculate velocity-position correlation function
   *
   * Arguments:
   *  py_coord   : Array of the timesteps
   *  py_rm      : Array of the rangematrix
   *  py_results : Array for vel-pos correlation, result
   *  py_absres  : Array for vel-pos correlation calculated with absolute value, result
   *  py_norm    : Array for normalization of results
   *  frame      : Timestep to calculate 
   *  dr         : width of the bins
   *
   */
  PyArrayObject *py_coord; // Array of the timesteps
  PyArrayObject *py_rm; // Rangematrix 
  PyArrayObject *py_results; // Results dotsum
  PyArrayObject *py_absres; // Results dotsum
  PyArrayObject *py_norm; // Results dotsum
  
  int frame;
  double dr;
  
  if (!PyArg_ParseTuple(args,"OOOOOdi",&py_coord,&py_rm,&py_results,&py_absres,&py_norm,&dr,&frame))
    return NULL;
 
  double *rm = PyArray_DATA(py_rm);
  double *coord = PyArray_DATA(py_coord);
  double *res = PyArray_DATA(py_results);
  double *absres = PyArray_DATA(py_absres);	
  long *norm = PyArray_DATA(py_norm);
  
  int noa = PyArray_SHAPE(py_coord)[1];
  int maxi = PyArray_SHAPE(py_results)[0];
  int bindex;

  printf("WARNING!: One should check that results are Ok! As code seems to have bug and it is not fixed yet\n");
  for(int i=0;i<noa;i++){
    for(int j=0;j<noa;j++) {
      if(i != j) {
	bindex = (int) (rm[j+noa*i]/dr);
	if (bindex > maxi-1) {
	  bindex = maxi-1;
	}
	// Not sure does this calculate velocity-position dot product correctly.
	// It seems to calcualte velocity-velocity dot product 
	absres[bindex]=absres[bindex]+fabs(dotp(&coord[frame*noa*6+i*6+3],&coord[frame*noa*6+j*6+3]));
	res[bindex]=res[bindex]+dotp(&coord[frame*noa*6+i*6+3],&coord[frame*noa*6+j*6+3]);
	norm[bindex]+=1;
      }   
    }
  }
  Py_RETURN_NONE;  
}



PyObject* rrcorr_frame(PyObject* self, PyObject *args)
  
{
  /*
   * Calcualte r1-r2 correlation 
   *
   * Arguments:
   *  py_coord : Array of the coordinates
   *  py_latt  : Array of the lattice vectors
   *  py_rm    : Array of the range matrix
   *  py_file  : File to write results 
   *  t        : Timestep for calculation
   * rmax      : cutoff of neighbors
   * crit      : Criteria for the angles
   *
   */
    PyArrayObject *py_coord; // Array of the timesteps
    PyArrayObject *py_latt; //lattice vectors
    PyArrayObject *py_rm;  //Rangematrix
    PyObject *py_file; // File to write results
    int t; // timestep for calculation
    double rmax; // Cutoff of neighbours
    double crit; // Criteria for the angle
    if (!PyArg_ParseTuple(args,"OOOOdid",&py_coord,&py_latt,&py_rm,&py_file,&rmax,&t,&crit))
        return NULL;

    double *coord = PyArray_DATA(py_coord);
    double *latt = PyArray_DATA(py_latt);
    double *rm = PyArray_DATA(py_rm);

    int noa = PyArray_SHAPE(py_coord)[1];

    int MAXNN = 100; // This is not good as now we have hard coded maximum number of neighbours
    int i,j,l,nj,nk;
    int ngbrs[MAXNN];
    int nn;
    double norm_pij,norm_pik;
    double d,dotpp,angle;
    double pij[3],pik[3];

    char numtostr[6];
    
    
    for(i=0;i<noa;i++)
    {
        for(j=0;j<MAXNN;j++) // Reset ngbrs 
        {
            ngbrs[j]=-1;
        }
	
        nn = 0;
        for(j=0;j<noa;j++)
        {
            if(i==j) continue;
            if(rm[i+noa*j] < rmax)
            {
                ngbrs[nn]=j;
                nn+=1;
            }
        } // neighbours list read
 

        for(nj=0;nj<nn;nj++) // Go trought all neighbours
        {
            if(ngbrs[nj] == i || ngbrs[nj] == -1) continue;
            avec(coord,latt,pij,i,ngbrs[nj],noa,t);
            d = 0.0;
            for(l=0;l<3;l++) // Calculate norm of pij
            {
                d+=pij[l]*pij[l];
            }
            norm_pij = sqrt(d);
	    
	    
            for(nk=0;nk<nn;nk++)
            {
                if (ngbrs[nk]==i || ngbrs[nk]==ngbrs[nj] || ngbrs[nk]==-1) continue;
                avec(coord,latt,pik,i,ngbrs[nk],noa,t);
                d = 0.0;
                for(l=0;l<3;l++) // Calculate norm of pik
                {
                    d+=pik[l]*pik[l];
                }
                norm_pik = sqrt(d);
		
                // calculate dot product
                dotpp = 0.0;
                for(l=0;l<3;l++)
                {
                    dotpp+=pik[l]*pij[l];
                }
                angle = acos(dotpp/(norm_pij*norm_pik));
                angle = angle*180.0/M_PI;
		
		if (angle>crit) {
		  

		  snprintf(numtostr,sizeof(numtostr),"%6.4f",rm[i+noa*ngbrs[nj]]);
		  //sprintf(numtostr,"%6.4f",rm[i+noa*ngbrs[nj]]);  // This is not save as can write over the buffer easilly 
		  PyFile_WriteString(numtostr,py_file);
		  PyFile_WriteString("  ",py_file);
		  
		  snprintf(numtostr,sizeof(numtostr),"%6.4f",rm[i+noa*ngbrs[nk]]);
		  PyFile_WriteString(numtostr,py_file);
		  PyFile_WriteString("  ",py_file);	

		  snprintf(numtostr,sizeof(numtostr),"%6.3f",angle);
		  PyFile_WriteString(numtostr,py_file);
		  PyFile_WriteString("\n",py_file);		
		  
		}

            }
        }
    }
  Py_RETURN_NONE;
}

static PyMethodDef functions[] = {
  {"orderparam_frame", (PyCFunction) Orderparam_frame, METH_VARARGS, "Calculate order parameter"},	
  {"correlation", (PyCFunction) correlation, METH_VARARGS, "Calculate correlation"},
  {"correlation_vec", (PyCFunction) correlation_vec, METH_VARARGS, "Calculate correlation for vector"},
  {"parcurrent", (PyCFunction) particlecurrents, METH_VARARGS, "Calculate particle current"},
  {"microdens", (PyCFunction) microdens, METH_VARARGS, "Calculate microscopic density"},
  {"vanHove", (PyCFunction) vanHove, METH_VARARGS, "Calculate vanHove function"},
  {"vanHove_onlyself", (PyCFunction) vanHove_onlyself, METH_VARARGS, "Calculate self-vanHove function"},
  {"pairc", (PyCFunction) pairc_c, METH_VARARGS, "Calculate pair correlation function"},
  {"autocorr_atom", (PyCFunction) vv_autocorr_atom_c, METH_VARARGS, "Autocorrelation function for atoms"},
  {"movetooriginal", (PyCFunction) movetooriginal_c, METH_VARARGS, "Move atoms to simulation cell"},
  {"rngmatrix", (PyCFunction) rngmatrix_c, METH_VARARGS, "Calculate range matrix"},
  {"rmperc", (PyCFunction) rmperc_c, METH_VARARGS, "Range matrix for percolation"},
  {"ecn_frame_c",(PyCFunction) ecn_frame_c,METH_VARARGS,"Effective coordinate number"},
  {"bondangles",(PyCFunction) bondangles_c,METH_VARARGS,"Bondangles"},
  {"velposcor",(PyCFunction) velposcor, METH_VARARGS,"vel-pos correlation"},
  {"rrcorr_frame",(PyCFunction) rrcorr_frame, METH_VARARGS,"r1-r2 correlation"},
  {NULL, NULL, 0, NULL}
};



static struct PyModuleDef analyse =
{
    PyModuleDef_HEAD_INIT,
    "analyse", /* name of module */
    "",          /* module documentation, may be NULL */
    -1,          /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
    functions
};


PyMODINIT_FUNC PyInit_analyse(void)
{
    return PyModule_Create(&analyse);
}

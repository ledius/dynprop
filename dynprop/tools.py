# -*- coding: utf-8 -*-
#! /usr/bin/env python3

"""
Tools for further analysing the results from trajectory class

Author: Matti Ropo, matti.ropo@tut.fi

"""

import numpy as np
from numpy import linalg as LA

def get_spectrum(A,qset,tmesh,window=None):
    """ (array,array, array,float) -> (array,array)
        
    Calculates FFT in timespace for array A. It is used to calculate
    for example dynamic structure factor from intermediate scattering function.

    Arguments:
    A      : Array for which specrum is calculated
    qset   : array of q vectors lengths
    tmesh  : time mesh for the ISF
    window : Possible smoothing window for FFT if None no
                 smoothing window is used. In meV.

    Returns:
    B     : array for dynamic structure factor
    wmesh : array for omege mesh in 1/t units.         

    """
        
    delta = tmesh[1]-tmesh[0]
        
    s = [slice(None)]*A.ndim
    s[0] = slice(-1,0,-1)

    wmesh = np.fft.fftshift(np.fft.fftfreq(2*len(tmesh)-1,delta))
    A = np.concatenate((A[s],A),axis=0)

        
    # Initialize array for S(q,w)
    B = np.zeros((A.shape[0],len(qset)))
    mu = 0.0

    if not window == None:
        mev2thz = 0.24180 # meV to Thz
        sigma = mev2thz*window/(1000*np.pi*2.0) # Thz to cm^-1
        f_w = (1.0/(sigma*np.sqrt(2.0*np.pi)))*np.exp(-0.5*((wmesh-mu)/sigma)**2)
        w = np.fft.fftshift(np.abs(np.fft.ifft(f_w))/delta)
        for q in range(len(qset)):
            B[:,q] = np.fft.fftshift(np.abs(np.fft.fft(A[:,q]*w)))*delta
    else:
        for q in range(len(qset)):
            B[:,q] = np.fft.fftshift(np.abs(np.fft.fft(A[:,q])))*delta

    return B, wmesh



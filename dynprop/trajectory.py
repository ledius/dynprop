# -*- coding: utf-8 -*-
#! /usr/bin/env python3

"""
Trajectory class read and analyze given trajectory file.

At the moment support: CPMD (and partially xyz) trajectories.

Author: Matti Ropo, matti.ropo@tut.fi

"""


import numpy as np
from numpy import linalg as LA
import dynprop.analyse as analyse


# Conersion constants
BTOA =  0.52917720859  # Bohrs to Angstroms
ATU2FS = 0.02418884326505 # Atomic time units to femto seconds

#Default  cutoffs

# In Ånsgtröms
SCUTOFF   = 3.00 # For short bonds for Sb
NNCUTOFF  = 3.75 # for nearest neighbours for Sb
NNNCUTOFF = 5.0  # for next nearest neighbours for Sb


class Trajectory:
    """ Contains information about molecular dynamics trajectory """

    def __init__(self,filename="TRAJECTORY",geofile="GEOMETRY.xyz",\
                 input="input",pp=False,sc=True,format='cpmd'):
        """ (str,str,bool,bool,str) -> (object)

        Initialize trajectory instance

        Arguments:
        filename: Name of trajectory file
        geofile: Name of xyz file to read types and number of atoms for
                 CPMD trajectory
        input: Name of CPMD input file needed for reading supercell
        sc : Move atoms to original supercell
        pp : Print progress of reading trajectories
        format: string to select trajectory file format, cpmd or xyz

        Return:
        Instance

        """

        self.velocities = False
        self.latticev = np.zeros((3,3)) # Lattice vectors
        self.tstep = None
    
        if format == "cpmd":
            self.timesteps,self.number_of_atoms,self.atoms = self._readcpmd(filename,geofile,input,pp)
            self.velocities = True
        elif format == "xyz":
            self.timesteps,self.number_of_atoms,self.atoms = self._readxyz(filename,input)  
        else:
            print("Uknown format: "+format)
            print("Currently support: cpmd and xyz files")
            exit()

        self.types = set(self.atoms) # set of atoms types in trajectory
        self.number_of_steps = len(self.timesteps)
        if sc:
            self._movetooriginal_c()

            
    def _latticevectrosfromcellparam(self,cell,symmetry):
        """ (list,int) -> list

        Construct CPMD style lattice vectors from cell parameters
        and symmetry.  

        Arguments:
        cell: list of cell parameter ( len of 6)
        symmetry: give symmetry of system and is used to construct lattice vectors

        Returns:
        lv : Lattice vectors, (3,3) nnumpy array
        
        """

        if not len(cell) == 6:
            raise RuntimeError("Cell parameter list is too long or short!")
        
        lv = np.zeros((3,3))

        cosa = cell[3]
        cosb = cell[4]
        cosg = cell[5]
        sina = np.sqrt(1-cosa*cosa)
        sinb = np.sqrt(1-cosb*cosb)
        sing = np.sqrt(1-cosg*cosg)
        a = cell[0]
        ba = cell[1] # b/a 
        ca = cell[2] # c/a 

        # CPMD seem to need different constructors for each symmetry
        if symmetry == 1: #Cubic symmetry
            lv[0]=[a,0.0,0.0]
            lv[1]=[0.0,a,0.0]
            lv[2]=[0.0,0.0,a]
        elif symmetry == 14: # Triclinic symmetry
            lv[0] = [a,0.0,0.0]
            lv[1] = [ba*a*cosg,ba*a*sing,0.0]
            term = np.sqrt((1.0+2*cosa*cosb*cosg-cosa**2\
                            -cosb**2-cosg**2)/(1.0-cosg**2))
            lv[2] = [ca*a*cosb,ca*a*(cosa-cosb)*cosg/sing,ca*a*term]
        
        return lv
        
    def _readcpmdinputfile(self,filename):
        """ (str) ->(list,int)
        
        Read CPMD input file and get lattice parameters and symmetry

        Arguments:
        filename : Filename for CPMD input file

        Return:
        cell parameters: list
        symmetry: number of symmetry group

        """
        # Check cell from input file. Does read symmetry and cell parameters
        ifile = open(filename,"r")

        lines = ifile.readlines()
        ifile.close()
        
        insystem = False
        incpmd = False
        inangstroms = False
        readcell = False
        readsymmetry = False
        for i in range(len(lines)):
            line = lines[i]

            if  "&CPMD" in line:
                incpmd = True
            if incpmd and "&END" in line:
                incpmd = False
            if incpmd:
                if "TIMESTEP" in line:
                    timestep = int(lines[i+1])
            if "&SYSTEM" in line:
                insystem = True
            if insystem and "&END" in line:
                insystem = False
            if insystem:
                if readcell:
                    cell = [float(x) for x in line.split()]
                    readcell = False
                if readsymmetry:
                    symmetry = int(line)
                    readsymmetry = False
                if "ANGSTROM" in line:
                    inangstroms = True
                elif "CELL" in line:
                    readcell = True
                elif "SYMMETRY" in line:
                    readsymmetry = True
        if not inangstroms:
            for i in range(3):
                cell[i] = cell[i]*BTOA
                
        return cell,symmetry,timestep

        
    def _readcpmd(self,filename="TRAJECTORY",geofile="GEOMETRY.xyz",\
                  inputfile="input",print_progress=False):
        """ (str,str,str,bool) -> list, int,list

        Open CPMD TRAJECTORY file and split it to time steps and use
        GEOMETRY.xyz to help in parsin the TRAJECTORY
        Returns the list of time steps
        
        Change Bohrs to Angstroms for internal use

        Arguments:
        filename : CPMD trajectory name
        geofile  : xyz file for reading types of atoms
        inputfile: CPMD input file for reading cell parameters
        print_progress : Do we print progress of reading the file

        Returns:
        tsteps : numpy array containing coord. for each frame and atoms
        noa    : number of atoms in simulation
        atoms  : Type of each atom 
        
        """

        if not inputfile  == None:
            # Read CPMD input file to get lattice parameters
            cell, symmetry,tstep = self._readcpmdinputfile(inputfile)
            self.tstep = tstep*ATU2FS
            # Construct lattice vectros from CELL parameters
            self.latticev = self._latticevectrosfromcellparam(cell,symmetry)
        
        # Check number of atoms in the system and atom type of each atom
        # from the provided xyz file.
        gfile = open(geofile,"r")

        num_of_atoms = int(gfile.readline())
        gfile.readline()
        step_types = []
        for line in gfile:
            step_types.append(line.split()[0])
        gfile.close()

        if len(step_types) != num_of_atoms:
            print("ERROR while reading "+geofile+".")
            print("Number of atoms and read lines is different!")
            print("number of atoms:"+str(num_of_atoms)+" lines: "+str(len(step_types)))
            exit()        

        # open the trajectory file
        tfile = open(filename,"r")

        atoms = []
        tsteps = []
        step_atoms = []
        linecount = 0
        for line in tfile:
            if "NEW DATA" in line:
                continue
            linecount = linecount + 1
            # change to A and fs units
            splitted = [float(x)*BTOA for x in line.split()[1:4]]
            splitted = splitted + [float(x)*BTOA/ATU2FS for x in line.split()[4:7]]
            step_atoms.append(splitted[0:6])
           
            # Increase counter for type of atoms
            if linecount % num_of_atoms == 0:
                # Sanity check that we have correct number of atoms in this time step
                if len(step_atoms) != num_of_atoms:
                    print("ERROR while reading", filename)
                    print("Number of atoms in time step one time step is different than it should be!")
                    exit()
                #Append new timestep to the list of timesteps
                tsteps.append(step_atoms)
                # Reset current step_atoms and type_counter
                step_atoms = []
            if print_progress:
                print("Read line: "+str(linecount))

        tfile.close()
        
        return np.array(tsteps),num_of_atoms,step_types

    def _readxyz(self,filename,cpmdinput=None):
        """ (str,str) -> list,int,num
        
        Read xyz trajectory file.

        Arguments:
        filename  : xyz trajectory file
        cpmdinput : if CPMD input file is given read cell parameters
        
        Returns:
        tsteps : numpy array containing coord. for each frame and atoms
        noa    : number of atoms in simulation
        atoms  : Type of each atom 

        """

        if not cpmdinput == None:
            # Read CPMD input file to get lattice parameters
            cell, symmetry = self._readcpmdinputfile(inputfile)
            # Construct lattice vectros from CELL parameters
            self.latticev = self._latticevectrosfromcellparam(cell,symmetry)
        
        # Read xyz file 
        xyzfile = open(filename,"r")
        num_of_atoms=int(xyzfile.readline())        
        nol = num_of_atoms+2 #Number of lines in step
        xyzfile.seek(0) # Retrun back to start
        tsteps = []
        atoms = []
        step_types = []
        step_atoms = []
        linecount = 0
        atomline = 0
        # Next loop over all the steps in file
        for line in xyzfile:
            linecount += 1
            atomline += 1              
            if atomline == 1 or atomline == 2:
                pass
            else:
                splitted = line.split()
                step_types.append(splitted[0]) 
                coords = [float(x) for x in splitted[1:4]]
                # Add xzeros as speed for consistency with CPMD trajectory files
                step_atoms.append(coords+[0.0,0.0,0.0]) 
            if linecount % nol == 0:
                # Sanity check that we have correct number of atoms in this time step
                if len(step_atoms) != num_of_atoms:
                    print("ERROR while reading", filename)
                    print("Number of atoms in time step one time step is different than it should be!")
                    exit()                
                #Append new timestep to the list of timesteps
                tsteps.append(step_atoms)
                # Reset current step_atoms and type_counter
                step_atoms = []
                atomline = 0
                if atoms == []:
                    atoms.append(step_types)
                step_types= []

        xyzfile.close()
        
        return np.array(tsteps),num_of_atoms,atoms            
         
    def _movetooriginal_c(self):
        """ (self) -> (None)
            
            Move atoms back to original supercell in all frames

        Arguments:
        None

        Returns:
        None
        """

        # Sanity check for lattice vectors
        if np.any([np.all(x == [0,0,0]) for x in self.latticev]):
            print("Some of the lattice vectors are zero. Cannot move atoms to the orginal cell")
            return
        
        tmatrix = np.matrix('0.0 0.0 0.0;0.0 0.0 0.0;0.0 0.0 0.0')
        tmatrix[:,:] = self.latticev.T
        inv_tmatrix = np.linalg.inv(tmatrix)

        analyse.movetooriginal(self.timesteps,tmatrix,inv_tmatrix,self.number_of_atoms,self.number_of_steps)


    def _calculatevelocities_py(self,force=False):
        """ (self) -> (None)
        Calculate velocites from the atomic positions.
        Velocities are accurete to O(dt^2)

        Arguments:
        force : Do we recalculate velocities from the positions.

        Returns:
        None
        """

        if self.velocities and not force:
            print("Velocities already calculated and not forced to recalculate!")
            return 

        if len(self.timesteps) < 2:
            print("Unable to calculate velocities too few steps!")
            return

        deltat = 1.0
        # First frame
        self.timesteps[0][3:6] = (self.timesteps[1][0:3]-self.timesteps[0][0:3])/delta
        # Last frame
        self.timesteps[0][3:6] = (self.timesteps[-1][0:3]-self.timesteps[-2][0:3])/delta
        
        for t in range(1,len(self.timesteps)-1):
            self.timesteps[t][3:6] = (self.timesteps[t+1][0:3]-self.timesteps[t-1][0:3])/2*delta
            
    def _movetooriginal_py(self):
        """ (self) -> (None)
        
        Move coordinates of atoms back to original supercell in all
        frames. Use python and is slow.

        Arguments:
        None

        Returns:
        None
        """
        print("Moving to original supercell")
        for t in range(self.number_of_steps):
            self._movetooriginal_frame(t)

    
    def _movetooriginal_frame(self,t):
        """ (self,int) -> (None)
            
        Move atoms back to original supercell in one frame. 
        This is a python version and is slow compared to c version.

        Argument:
        t : timesteps

        Returns:
        None

        """

        tmatrix = np.matrix('0.0 0.0 0.0;0.0 0.0 0.0;0.0 0.0 0.0')
        tmatrix = self.latticev.T
        inv_tmatrix = np.linalg.inv(tmatrix)
         
        for i in range(self.number_of_atoms):
            vec = self.timesteps[t][i][0:3]
            
            # Transform to local coordinates
            b_vec = np.dot(inv_tmatrix,vec)
            #  Translate coordinates to original supercell
            for a in range(3):
                b_vec[a] = b_vec[a]%1
            # Transform back to original coordinates
            n_vec = np.dot(tmatrix,b_vec.T)

            # Replace original coordinates
            for a in range(3):
                self.timesteps[t][i][a] = n_vec[a]

        
    def arng(self,t,ri,ai):
        """ (self,int,int,int) -> float

        Calculates range between teo atoms in timesteps t.
        Takes account periodic boundary conditions.

        Arguments:
        t  : timestep
        ri : index of first atom
        ai : index of second atom

        Returns:
        rng : distance between two atoms
 
        """
        
        nvec=np.zeros((1,3))
        rng = 100.0
        # Asume that atoms aren't furthen than in the next cell
        for ni in [-1,0,1]:
            for nj in [-1,0,1]:
                for nk in [-1,0,1]:
                    nx = (ni*self.latticev[0][0]+nj*self.latticev[1][0]+nk*self.latticev[2][0])
                    ny = (ni*self.latticev[0][1]+nj*self.latticev[1][1]+nk*self.latticev[2][1])
                    nz = (ni*self.latticev[0][2]+nj*self.latticev[1][2]+nk*self.latticev[2][2])
                    nvec = [nx,ny,nz]
                    ds = self.timesteps[t][ri][0:3]-self.timesteps[t][ai][0:3]+nvec
                    sumds = np.sqrt(sum(ds**2))
                    if rng > sumds:
                        rng = sumds

        return rng

    def shortvec(self,t,ri,ai):
        """ (self,int,int,int) -> list

        Calculate distance vector between two atoms in timestep t. 
        Takes account the periodic boundary conditions

        Arguments:
        t  : timestep
        ri : index of first atom
        ai : index of second atom

        Returns:
        dv : Vector between ai and ri atoms
        """

        nvec=np.zeros((1,3))

        rng = 100.0
        dv = [0,0,0]
        # Asume that atoms aren't furthen than in the next cell
        for ni in [-1,0,1]:
            for nj in [-1,0,1]:
                for nk in [-1,0,1]:
                    nx = (ni*self.latticev[0][0]+nj*self.latticev[1][0]+nk*self.latticev[2][0])
                    ny = (ni*self.latticev[0][1]+nj*self.latticev[1][1]+nk*self.latticev[2][1])
                    nz = (ni*self.latticev[0][2]+nj*self.latticev[1][2]+nk*self.latticev[2][2])
                    nvec = [nx,ny,nz]
                    ds = self.timesteps[t][ri][0:3]-self.timesteps[t][ai][0:3]+nvec
                    sumds = np.sqrt(sum(ds**2))
                    if rng > sumds:
                        rng = sumds
                        dv = ds
        return np.array(dv) # return vector (np.array)
  
    def _get_rangematrix_py(self,t):
        """ (self,i) -> (np.array)

        Calculate range matrix for one time step of the system       

        Arguments:
        t : timestep
        
        Returns:
        rngmatrix : range matrix of time step t
        """
        
        rngmatrix = np.zeros((self.number_of_atoms,self.number_of_atoms))

        for i in range(self.number_of_atoms):
            for j in range(i+1,self.number_of_atoms):
                rng = self.arng(t,i,j)
                rngmatrix[i,j]=rng
                rngmatrix[j,i]=rng

        return rngmatrix

    def _get_rangematrix(self,t):
        """ (self,i) -> (np.array)
        Calculate range matrix for one time step of the system
        
        Arguments:
        t  : timesteps which range matrix is calculated
        
        Returns:
        rngmatrix : range matrix of time step t
        """
        
        lattm = np.zeros((3,3))
        # construct lattice matrix from lattice vectros
        lattm[:,0] = self.latticev[0]
        lattm[:,1] = self.latticev[1]
        lattm[:,2] = self.latticev[2]
        
        rngmatrix = np.zeros((self.number_of_atoms,self.number_of_atoms))
        analyse.rngmatrix(self.timesteps,lattm,rngmatrix,self.number_of_atoms,t)

        return rngmatrix

    def get_rm_percolation(self,t,c = False,a=False,b = False):
        """ (self,i) -> (np.array)
        Calculate rangematrix for percolation test.
        Matrix is calculated  by taking account periodic boundary conditions only
        to positive direction.

        Arguments:
        t  : Timestep
        a  : Do we take account A lattice vector
        b  : Do we take account B lattice vector
        c  : Do we take account C lattice vector

        Returns:
        rngmatrix : Range mattrix between atoms for calculation of percolation
        """

        lattm = np.zeros((3,3))

        if a:
            lattm[:,0] = self.latticev[0]
        else:
            lattm[:,0] = [0.0,0.0,0.0]
        if b:
            lattm[:,1] = self.latticev[1]
        else:
            lattm[:,1] = [0.0,0.0,0.0]
        if c:
            lattm[:,2] = self.latticev[2]
        else:
            lattm[:,2] = [0.0,0.0,0.0]
            
        rngmatrix = np.zeros((self.number_of_atoms,self.number_of_atoms))
        analyse.rmperc(self.timesteps,lattm,rngmatrix,self.number_of_atoms,t)

        return rngmatrix

    def get_numberofshort(self,t):
        """ (self,int) -> list
        Calculate number of neighbours with short connection                                                       
        for all atoms in timstep t                                                                          
        
        Arguments:
        t : timestep

        Returns:
        results : Indeces of the neighbouring atoms within the SCUTOFF 
        """

        rngmatrix = self.get_rangematrix(t)
        results = []
        for i in range(self.number_of_atoms):
            nearest,nextnearest = self.get_neighbours_atom(i,rngmatrix)
            shorts = [x for x in nearest if rngmatrix[i,x]<SCUTOFF]
            results.append(len(shorts))
        return results

    def get_neighbours_atom(self,i,rm):
        """ (self,int,np.matrix) -> (list,list)
        Get nearest and next nearest neighbours of atom i
        using the range matrix for timestep t.
        
        Arguments:
        i  : index of atom i
        rm : pre-calculated rangematrix used to get neighbor

        Returns
        nearest : list of indeces of nearest atoms within NNCUTOFF
        nextnrearest: list of indeces of next nearest atoms within NNNCUTOFF
        """

        nearest = []
        nextnearest = []

        for j in range(self.number_of_atoms):
            if i == j:
                pass
            elif rm[i,j] < NNCUTOFF:
                nearest.append(j)
            elif rm[i,j] < NNNCUTOFF:
                nextnearest.append(j)

        return nearest,nextnearest


    def get_bond_angle_distribution_frame_py(self,t,cutoff = NNCUTOFF,pp=False):
        """
        Calculate bond angle for one frame
        Python version (slow)

        Arguments:
        t      : time step
        cutoff : cutoff range for bonded atoms
        pp     : Do we print progress of the calculation

        Return:
         list: bond angles
        """

        rm = self.get_rangematrix(t)

        angles = []
        ngbrs = []
        for i in range(self.number_of_atoms):
            if pp:
                print("Atom: %i /%i" %(i+1,self.number_of_atoms))
            # Get indeces of neighbrours under cutoff
            for j in range(self.number_of_atoms):
                if i == j:
                    continue
                elif rm[i,j] < cutoff:
                    ngbrs.append(j)
            # Calculate bonds angles between atom and its neighbours
            for j in ngbrs:
                if i == j:
                    continue
                pij = self.shortvec(t,i,j)
                #norm_pij = np.linalg.norm(pij)
                norm_pij = np.sqrt(pij.dot(pij))
                for k in ngbrs:
                    if j == k or i == k :
                        pass
                    else:
                        pik = self.shortvec(t,i,k)
                        #norm_pik = np.linalg.norm(pik)
                        norm_pik = np.sqrt(pik.dot(pik))

                        # Calculate angle between vectors ie. bond angle
                        angle = np.degrees(np.arccos(np.dot(pij,pik)/(norm_pik*norm_pij)))
                        angles.append(angle)
        return angles

    def get_bond_angle_distribution_frame(self,t,cutoff = NNCUTOFF):
        """
        Calculate bond angle for one frame using C function

        Arguments:
        t      : time step
        cutoff :  cutoff range for bonded atoms

        Return:
        angles : list of bond angles 
        """

        rm = self.get_rangematrix_c(t)

        # Prepare the lattice vector matrix
        lattm = np.zeros((3,3))
        lattm[:,0] = self.latticev[0]
        lattm[:,1] = self.latticev[1]
        lattm[:,2] = self.latticev[2]

        angles = []
        analyse.bondangles(self.timesteps,lattm,rm,angles,cutoff,self.number_of_atoms,t)
        return angles

    def get_bond_angles_distribution_frame_varcutoff(self,t,cutoffs):
        """ (int, list) -> (list)
        Calculate distribution of angles for one frame for list of 
        cutoffs and returns list of angles list for those cutoffs.
        This way we need to calculate range matrix for each frame obly once
        for all cutoffs
        
        Arguments:
        t       : time step
        cutoffs : list of cutoffs 

        Return:
        angles : list of bond angle lists for cutoffs
        """

        rm = self.get_rangematrix_c(t)

        lattm = np.zeros((3,3))
        lattm[:,0] = self.latticev[0]
        lattm[:,1] = self.latticev[1]
        lattm[:,2] = self.latticev[2]

        angles = []
        for c in cutoffs:
            a = []
            analyse.bondangles(self.timesteps,lattm,rm,a,c,self.number_of_atoms,t)
            angles.append(a)
        return angles

    def rrcorrelation_frame(self,t):
        """ (int) -> (list)
        Calculate r1-r2 correlation for a frame

        Arguments:
        t  : timestep

        Returns:
        res : r1-r2 correlation function for frame t
        """

        rm = self.get_rangematrix_c(t)
        rmax = 4.0
        res = [[],[],[]]
        for i in range(self.number_of_atoms):
            ngbrs = []
            for j in range(self.number_of_atoms):
                if i == j:
                    continue
                elif rm[i,j] < rmax:
                    ngbrs.append(j)
            for ni in range(len(ngbrs)):
                rini = self.shortvec(t,i,ngbrs[ni])
                norm_ni = np.sqrt(rini.dot(rini))
                for nj in range(ni+1,len(ngbrs)):
                    rinj = self.shortvec(t,i,ngbrs[nj])
                    norm_nj = np.sqrt(rinj.dot(rinj))
                    # Calculate angle between vectors ie. bond angle
                    angle = np.degrees(np.arccos(np.dot(rini,rinj)/(norm_ni*norm_nj)))
                    if angle > 150.0:
                        res[0].append(rm[i,ngbrs[ni]])
                        res[1].append(rm[i,ngbrs[nj]])
                        res[2].append(angle)

        return res

    def rrcorrelation(self):
        """
	Calculate histogram for r1-r2 correlation for trajectory using python function

        Arguments:
        None

        Returns:
        bins  : Bins for correlation values
        rbins : list of bin centrals
        """
      
        minr = 2.6
        maxr = 4.0
        dr = 0.2
        # Constrcut bins in 2d
        rbin = np.arange(minr,maxr,dr)
        nb = len(rbin)
        bins = np.zeros((nb,nb),dtype=np.int)

      
        for t in range(self.number_of_steps):
            res = self.rrcorrelation_frame(t)
            # bin results
            for i in range(len(res[0])):
                rx = res[0][i]
                ry = res[1][i]
                ix = int ((rx-minr)/dr)
                iy = int ((ry-minr)/dr)
                bins[ix][iy]+=1

        return bins, rbin


    def rrcorrelation_c(self,filename = "rrcor.dat",maxr=4.0,start=None,end=None,step=1):
        """ 
	Calculate r1 r2 colleration for trajectory using C routine.
        Write results to text file

        Arguments:
        filename : filename to save r1-r2 correlation
        maxr     : maximum range to calcualte correlation between atoms
        start    : start frame
        end      : end frame
        step     : Step lenght in frames

        Returns:
        None
        """

        if start == None:
            start = 0
        if end == None:
            end = self.number_of_steps

        lattm = np.zeros((3,3))
        lattm[:,0] = self.latticev[0]
        lattm[:,1] = self.latticev[1]
        lattm[:,2] = self.latticev[2]
       
        fh = open(filename,"w")
        for t in range(start,end,step):
            print("Frame %i" %(t+1))
            rm = self.get_rangematrix_c(t) 
            analyse.rrcorr_frame(self.timesteps,lattm,rm,fh,maxr,t)
        fh.close()
     

    def neighbour_distribution_frame(self,t):
        """
        Calculate distribution of 12 nearest neighbour of each atom for a frame
        
        Arguments:
        t : Timestep
        
        Returns:
        results : list of list of neighbours indeces
        """

        results = ([],[],[],[],[],[],[],[],[],[],[],[])
        rm = self.get_rangematrix_c(t)
        for i in range(self.number_of_atoms):
            r = rm[i,:]
            r.sort()
            nns = list(r[1:13]) # 12 Neighbours list
            for j in range(len(nns)):
                results[j].append(nns[j])
        return results
        
    def neighbour_distribution(self,pp=False):
        """
        Calculate distribution of 12 nearest neighbours of trajectory

        Arguments:
        pp   : Do we print progress or not. (Boolean)

        Returns:
        results : distributions of 12 nearest neighbours 
        """
        
        results = ([],[],[],[],[],[],[],[],[],[],[],[])
        for t in range(self.number_of_steps):
            if pp:
                print("%i"%(t))
            res = self.neighbour_distribution_frame(t)
            for j in range(len(res)):
                results[j].extend(res[j])
        return results


    def bondlifetimes_test(self):
        """ (self) -> (list,list)
        Calculate life times of bonds of atoms in trajectory by analyzing 
        neigborh list pf each atom during trajectory.
        
        NOTE: Test function for development purposes
        """
 
        bondmatrix = [ [ [] for c in range(self.number_of_atoms) ] for r in range(self.number_of_steps) ]
        
        for t in range(0,self.number_of_steps):
            # Calculate rangematrix for timestep
            rm = self.get_rangematrix(t)
            # Calculate nearest neighbours
            for i in range(self.number_of_atoms):
                nn,nnn = self.get_neighbours_atom(i,rm)
                shorts = [x for x in nn if rm[i,x]<SCUTOFF]
                #if len(shorts)> 6:
                #    print("ERROR, len(shorts) > 6")
                #    print("If this happens we have to think something new")
                #    print("time: %i atom: %i" %(t,i))
                #    quit()
                bondmatrix[t][i].extend(shorts)
        
        # bond matrix is ready and we need to analyze it
        # Go throught one atom at time                       
        for i in range(self.number_of_atoms):
            current = [] # list to save current neighbours
            # first save the first neighbours        
            for b in bondmatrix[0][i]:
                current.append([b,0])
         
            #  go through other timesteps
            for t in range(1,self.number_of_steps):
                # Check if we have all old ones still
                for c in current:
                    bondbreak = True
                    for n in bondmatrix[t][i]:                        
                        if c[0] == n:
                            bondbreak = False
                            break
                        # We bond have broken and we print it and remove 
                        # it from current list
                    if bondbreak:
                        # Print types and lenght of the bond (in time steps)
                        #rint(i,c[0],t,c[1])
                        #print("%2s %2s %i" %(self.atoms[i],self.atoms[c[0]],t-c[1]))
                        btime = float(t-c[1])
                        print("%f" %( btime*3.02360540813125))
                        # Remove non existing bond from the list
                        current.remove(c)                        
                        
                # Check if new bonds are made and add them to the current list
                for n in bondmatrix[t][i]:
                    newbond = True
                    for c in current:
                        if c[0] == n:
                            newbond = False
                            break
                    if newbond:
                        # New bond is found add it to the current list
                        current.append([n,t])                 

    def bondlifetimes(self):
        """ (self) -> (list,list)
        
        Calculate bond lifetimes for system by analyzing
        neigbour list of atoms

        Arguments:
        None

        Returns:
        None
        
        NOTE: Not sure how much there is difference to test version.
        """

        bondmatrix = np.zeros((self.number_of_steps,self.number_of_atoms,6))
        for t in range(0,self.number_of_steps):
            # Calculate rangematrix for timestep
            rm = self.get_rangematrix_c(t)
            # Calculate nearest neighbours
            for i in range(self.number_of_atoms):
                nn,nnn = self.get_neighbours_atom(i,rm)
                shorts = [x for x in nn if rm[i,x]<SCUTOFF]
                for b in range(6):
                    if len(shorts)> 6:
                        print("ERROR, len(shorts) > 6")
                        print("If this happens we have to think something new")
                        print("time: %i atom: %i" %(t,i))
                        quit()
                    if len(shorts) > b:
                        bondmatrix[t,i,b] = shorts[b]
                    else:
                        bondmatrix[t,i,b] = -1

        # bond matrix is ready and we need to analyze it
        # Go throught one atom at time      
        blfile = open("bond_bl.dat","w") # NOTE: Need fix  ->  filename to argument
        for i in range(self.number_of_atoms):
            
            current = [] # list to save current neighbours
            # first save the first neighbours        
            for nn in bondmatrix[0,i,:]:
                if not nn == -1:
                    cnn = 6 - list(bondmatrix[0,nn,:]).count(-1)
                    current.append([nn,0,cnn]) # save id of neighbours and the time and number of shorts
            blen = [len(current),0]
         
            #  go through other timesteps
            for t in range(1,self.number_of_steps):
                # Get neighbours of timestep t
                new = []          
                for nn in bondmatrix[t,i,:]:
                    if not nn == -1:
                        cnn = 6 - list(bondmatrix[0,nn,:]).count(-1)
                        new.append([nn,t,cnn])
                if blen[0] != len(new):
                    blfile.write("%i %i\n" % (blen[0],t-blen[1]))
                    blen=[len(new),t]
 
               # Compare new list of neighbours and old one
#                current.sort()
#                new.sort()

                # Bond analysis                 
                # Check if some old bond are broken and remove them from the current list
                for c in current:
                    bondbreak = True
                    for n in new:                        
                        if c[0] == n[0]:
                            bondbreak = False
                            break
                        # We bond have broken and we print it and remove 
                        # it from current list
                    if bondbreak:
                        # print neighbour, when made,number of neighbours when conected,
                        #              me, when broken neighnours when broken
                        print("%i %i %i %i %i %i" %(c[0],c[1],c[2],i,t,len(current)))
                        current.remove(c)                        
                        
                # Check if new bonds are made and add them to the current list
                for n in new:
                    newbond = True
                    for c in current:
                        if c[0] == n[0]:
                            newbond = False
                            break
                    if newbond:
                        # New bond is found add it to the current list
                        current.append(n)

        blfile.close()

    def paircorrelation_step(self,t,dr=0.5,maxrng=50.0,final=True):
        """ (self) -> list        
        Calculate pair correlation function for a frame
        
        Arguments:
        t      : timestep to calculate for
        dr     : delta r for binning
        maxrng : Maximum distance which is taken account for calculation
        final  : Do we give final results with bins and numbers or partial results with counted occurence only

        Returns:
        results : list of bins and count on the bins
        
        NOTE: The what this function returns depend on the final argument.
              This is not good thing for as it can easily break the program! 
              Need FIXING!
        """
        
        
        #Construct bins and initialize counting list
        bins = np.arange(0.0,maxrng,dr)
        count = [0]*len(bins) 

        for i in range(self.number_of_atoms):
            for j in range(i,self.number_of_atoms):
                if not i == j:    
                    # We could use also rngmatrix
                    rng = self.arng(t,i,j)
                    if rng < maxrng:
                        ll = int(rng//dr)
                        count[ll]+=2 # calculate both pair from i to j and j to i
                
            
        # normalize with number of atoms in system
        #count =[float(i)/float(self.noa) for i in count]

        #volume 
        vol = self.latticev[0][0]*self.latticev[1][1]*self.latticev[2][2]
        
        # Normalize with volume
        for i in range(len(count)):
            r = (i+0.5)*dr
            norm = self.number_of_atoms*(self.number_of_atoms-1)*4*np.pi*r*r*dr
            count[i]=vol*float(count[i])/norm
        
        if final:
        # construct results pairs
            results = []
            for i in range(len(count)):
                results.append([bins[i],count[i]])
            return results
        else:
            return count    

    def paircorrelation_step_type(self,t,dr,maxrng,ainds,binds,final=True):
        """ (self) -> list        
        Calculate pair correlation function between atoms with indeces ainds and 
        binds for step t
        
        Arguments:
        t      : timestep to calculate for
        dr     : delta r for binning
        maxrng : Maximum distance which is taken account for calculation
        ainds  : indeces of the first atom type
        binds  : indeces of the second atom type
        final  : Do we give final results with bins and numbers or partial results with counted occurence only

        Returns:
        results : list of bins and count on the bins
        
        NOTE: The what this function returns depend on the final argument.
              This is not good thing for as it can easily break the program! 
              Need FIXING!
        """

        # First sainity checks
        if ainds == None:
            print("ERROR in paircorrelation_step_type: No ainds given!")
            raise
        if binds == None:
            print("ERROR in paircorrelation_step_type: No binds given!")
            raise
        
        
        #Construct bins and initialize counting list
        bins = np.arange(0.0,maxrng,dr)
        count = [0]*len(bins) 
        anorm = 0
        for i in ainds:
            for j in binds:
                if not i == j:
                    anorm += 1
                    # We could use also rngmatrix
                    rng = self.arng(t,i,j)
                    if rng < maxrng:
                        ll = int(rng//dr)
                        count[ll]+=2 # calculate both pair from i to j and j to i
                            
        # normalize with number of atoms in system
        #count =[float(i)/float(self.noa) for i in count]

        #volume 
        vol = self.latticev[0][0]*self.latticev[1][1]*self.latticev[2][2]
        
        # Normalize with volume
        for i in range(len(count)):
            r = (i+0.5)*dr
            #norm = self.number_of_atoms*(self.number_of_atoms-1)*4*np.pi*r*r*dr
            norm = float(anorm)*4*np.pi*r*r*dr
            count[i]=vol*float(count[i])/norm
        
        if final:
        # construct results pairs
            results = []
            for i in range(len(count)):
                results.append([bins[i],count[i]])
            return results
        else:
            return count    

    def paricorrelation_type_py(self,dr,maxrng=50.0,typea,typeb,start=None,end=None):
        """ (self,float,float) -> (list)
        Calculate pair correlation function for trajectory using python.
        
        Arguments:
        dr     : bin width for range
        maxrng : maximum range. Default 50.0 Å
        typea  : index or name of the atoms type a. If none then all atoms.
        typeb  : index or name of the atoms type b. If none then all atoms.
        start  : index of start frame
        end    : index of end frame

        Returns:
        results : pair colleration function
        """
        
        if start == None:
            start = 0
        if end == None:
            end = self.number_of_steps

        if typea == None:
            ainds = len(self.number_of_atoms)
        elif isinstance(typea,str):
            if not typea in self.types:
                print("ERROR No "+typea+" in trajectory")
                raise
            else:
                ainds = [i for i, x in enumerate(self.atoms) if x == typea]
        elif isintance(typea,int):
            if typea >= len(self.types):
                print("ERROR: typea "+str(typea)+" index larger than "+str(len(self.types)))
                raise
            else:
                ainds=[i for i, x in enumerate(self.atoms) if x == self.types[typea]]
        else:
            print("ERROR: wrong kind of typea!")
            raise

        if typeb == None:
            binds = len(self.number_of_atoms)
        elif isinstance(typeb,str):
            if not typea in self.types:
                print("ERROR No "+typeb+" in trajectory")
                raise
            else:
                ainds = [i for i, x in enumerate(self.atoms) if x == typeb]
        elif isintance(typea,int):
            if typeb >= len(self.types):
                print("ERROR: typea "+str(typeb)+" index larger than "+str(len(self.types)))
                raise
            else:
                binds=[i for i, x in enumerate(self.atoms) if x == self.types[typeb]]
        else:
            print("ERROR: wrong kind of typeb!")
            raise

        
        # Initialize bins and counter
        bins = np.arange(0.0,maxrng,dr)
        count = [0]*len(bins)
        for t in range(start,end):
            c = self.paircorrelation_step_type(t,dr,maxrng,aind,bind,False)
            count = list(map(sum, zip(count,c)))
                
        # Normalize counter with number of timesteps
        count =[float(i)/float(len(self.timesteps)) for i in count]        
        
        # construct results pairs
        results = []
        for i in range(len(count)):
            results.append([bins[i],count[i]])

        return results        

    def paircorrelation_py(self,dr,maxrng=50.0,start=None,end=None):
        """ (self,float,float) -> (list)
        Calculate pair correlation function for trajectory using python.
        
        Arguments:
        dr     : bin width for range
        maxrng : maximum range. Default 50.0 Å
        start  : index of start frame
        end    : index of end frame

        Returns:
        results : pair colleration function
        """
        
        if start == None:
            start = 0
        if end == None:
            end = self.number_of_steps
         
 
        # Initialize bins and counter
        bins = np.arange(0.0,maxrng,dr)
        count = [0]*len(bins)
        for t in range(start,end):
            c = self.paircorrelation_step(t,dr,maxrng,False)
            count = list(map(sum, zip(count,c)))
                
        # Normalize counter with number of timesteps
        count =[float(i)/float(len(self.timesteps)) for i in count]        
        
        # construct results pairs
        results = []
        for i in range(len(count)):
            results.append([bins[i],count[i]])

        return results        

    def paircorrelation(self,dr,maxrng=50.0):
        """ (self,float,float) -> list
        Calculates pair correlation function using c function
        
        Arguments:
        maxrng : maximum range to which is takeng account
        dr     : diwth of the bins for range

        """
        bins = np.arange(0.0,maxrng,dr)
        gr = np.zeros(len(bins))

       # Set up lattice matrix for C
        lattm = np.zeros((3,3))
        lattm[:,0] = self.latticev[0]
        lattm[:,1] = self.latticev[1]
        lattm[:,2] = self.latticev[2]           

        # Calculate pair correlation 
        analyse.pairc(self.timesteps,gr,lattm,len(bins), maxrng,dr, self.number_of_atoms,self.number_of_steps)           
 
        hist = np.copy(gr)
        rbins = []
        for i in range(len(bins)):
            r = (i+0.5)*dr
            rbins.append(r)
            shell_vol= 4.0*np.pi*(((i+1)*dr)**3-(i*dr)**3)/3.0
            norm = self.number_of_atoms*shell_vol
            gr[i]=(float(gr[i])/norm)/float(len(self.timesteps))
            
        # Retrun paircorrelation funciton, bin centrals and histogram
        return gr,np.array(rbins),hist

       

    def vanHovefunction(self,maxrange,dr,wstep = 5,wsize = None):
        """   (selg,float,float) -> (array, list, list)
        
        Calculate VanHove (time depend pair correlation fuction) function 
        from trajectory and return it with list of time and radial positions

        Arguments:
        maxrange   : Maximum range
        dr         : width of delta r
        wstep      : window step ie. how much window is moved after calculated
        wsize      : window size ie. how many frame are included each window

        Returns:
        Grt      : Time dependt pair correlation function
        Grts     : Time depend self pair correlation function
        rbins    : r central of bins
        timebins : time bins 
        hist     : Histogram of occurences in the bins 
        histS    : Histogram of occurance in the self-bins

        NOTE: Should add some more arguments to get self-only vanHove functions
              Need FIXING.
        """
        
        bins = np.arange(0.0,maxrange,dr)
        rbins = []
        timebins = []
        nbins = len(bins)
        #tbins = 10
        tbins = self.number_of_steps/2  
        if wsize == None:
            wsize = self.number_of_steps/2
        #Initialize matrix for vanHove function
        Grt = np.zeros((wsize,nbins),dtype=np.float64) # Distinct
        GrtS = np.zeros((wsize,nbins),dtype=np.float64) # Self
        
        # Set up lattice matrix for C
        lattm = np.zeros((3,3))

        lattm[:,0] = self.latticev[0]
        lattm[:,1] = self.latticev[1]
        lattm[:,2] = self.latticev[2]        
        # Call c program to do actual calculation this is parallellized with
        # openMP.
        selfonly=False
        if selfonly:
            analyse.vanHove_onlyself(self.timesteps,Grt,GrtS,lattm,nbins,
                                     maxrange,dr, self.number_of_atoms,
                                     tbins,wstep,wsize)            
        else:
            analyse.vanHove(self.timesteps,Grt,GrtS,lattm,nbins,maxrange,dr,
                        self.number_of_atoms,tbins,wstep,wsize)
       
        hist = np.copy(Grt)
        histS = np.copy(GrtS)
        # Calculate volume of cell for normalization 
        #vol = self.latticev[0][0]*self.latticev[1][1]*self.latticev[2][2]
        wstep_count = tbins/wstep + 1
        # Normalize results
        for t in range(wsize):
            # Save T mesh for output
            timebins.append(t*0.02418884326505*125)
            for i in range(nbins):
               r = (i+0.5)*dr
               shell_vol= 4.0*np.pi*(((i+1)*dr)**3-(i*dr)**3)/3.0
               norm = self.number_of_atoms*shell_vol

               Grt[t,i]=(float(Grt[t,i])/norm)/(float(wstep_count))
               GrtS[t,i]=(float(GrtS[t,i])/norm)/(float(wstep_count))     
               # float(tbins) should be the number of windows used in the calculation(in c code), not numberrrs of tbins 
               # Otherwise the normalization is not correct
        # Construct R mesh for output
        for i in range(nbins):
            r = (i+0.5)*dr
            rbins.append(r)        
        # Retrun 2 vanH functions, radial and time meshes
        return Grt,GrtS,rbins,timebins, hist, histS

    def microscopicdensity_frame(self,t,qvecs):
        """ (Trajectory,int,array) -> (np.array,np.array) 
        
        Calculate Fourier component of microscopic density for a frame 

        Arguments:
        t    : timestep
        qvec : array of qvectors
    
        Returns:

        rho : Fourier component of microscopic density for a frame
        """

        r = self.timesteps[t,:,0:3] # Positions of particles at time t
        qr = np.dot(qvecs,r.T) # dot product of q and r vectros
        
        exp = np.exp(np.complex(0,1)*qr) # Calculate exp part 
        rho = np.sum(exp,axis=1) # Sum over all R vectors in the frame
        norm = np.sqrt(self.number_of_atoms) # normalization 
        return rho/norm

    def microscopicdensity(self,qvecs):
        """ 
        Calculates Fourier component of miscroscopic densities for the trajectory

        Arguments:
        qvecs : array of q vectors

        Returns:
        rho : Fourier component of microscopic density
            
        """
        
        rho = np.zeros((self.number_of_steps,len(qvecs)),dtype=np.complex128)
        for t in range(0,self.number_of_steps):
            print("Microscopic density step: %i /%i" %(t+1,self.number_of_steps))
            rho_frame = self.microscopicdensity_frame(t,qvecs)
            rho[t,:] = rho_frame
        return rho
        
    def intermediatescatteringfunction(self,qvecs):
        """  (self) -> None 
        
        Calculates intermediate scattering function (ISF) for trajectory

        Arguments:
        qvecs : array of q vectors

        Returns:
        ISF   : array for intermediate scattering function
        qset  : list of norms of q vectors
        tmest : list of time mesh
            
        """
        # Sort qvectors asscending order
        qvecs = sorted(qvecs,key= lambda v: np.sqrt(np.dot(v,v)))
        qvecs =np.array(qvecs)

        qnorm = LA.norm(qvecs,axis=1) # Lenghts of the qvectors
        qnorm = np.around(qnorm,5)        
        qset = sorted(list(set(qnorm))) # Get ordered list of invidual norm values

        nos = self.number_of_steps
        noa = self.number_of_atoms
        wsize = 5000
        maxstep = nos -wsize

        python = True
        # get microsscopic density and norms of q vectors 
        if python:
            rho = self.microscopicdensity(qvecs)
        else:
            rho = np.zeros((self.number_of_steps,len(qvecs)),dtype=np.complex)
            analyse.microdens(self.timesteps,rho,qvecs,len(qvecs),noa,nos)
        #rho_old = self.microscopicdensity(qvecs)             
        ISF = np.zeros((wsize,len(qset)),dtype=np.float64)
        
        # Correlation could be made with numpy.correlate
        python = False
        tmesh = []
        if python:
            for r in range(maxstep):
                print("ISF step: %i/%i" %(r,maxstep))
                tmesh.append(r*0.02418884326505*125)
                for t in range(wsize):
                    corr_qvec = np.real(rho[t]*rho[r+t].conjugate())
                    #Collect correlation for each q-value
                    corr = []
                    for s in qset:
                        ind = np.where(qnorm == s)
                        corr.append(np.sum(corr_qvec[ind]))
                    ISF[t,:] = ISF[t,:] + corr
            ISF = ISF/maxstep
        else:
            # Construct matrix for qset indeces and helper matrix
            qset_i =[]
            qset_n = []
            for s in qset:
                ind = list(np.where(qnorm == s)[0])
                qset_n.append(len(ind))
                qset_i.append(ind)
            qset_max = len(sorted(qset_i,key=len)[-1])
            qset_i = np.array([xi+[-1]*(qset_max-len(xi)) for xi in qset_i])
            qset_n = np.array(qset_n)       
            analyse.correlation(rho,ISF,qset_i,qset_n,wsize,maxstep)
            
            for r in range(wsize):
                tmesh.append(r*self.tstep)

        return ISF,qset,tmesh



    def particlecurrent_frame(self,t,qvecs):
        """ (Trajectory,int,array) -> (np.array,np.array) 
        
        Calculate transverse and longitudal part of 
        particle current function for given qvectors and frame
        Python version.

        Arguments:
        t     : timestep
        qvecs : array of qvectors

        Returns:
        Jq_L : Longitudal part of particle current function of frame t
        Jq_T : Transverse part of particel current function of frame t
        """

        qnorm = LA.norm(qvecs,axis=1) # Norm of each q vector
        #La.norm can be replace with np.sqrt(x*x,axis=1) framework.
        
        r = self.timesteps[t,:,0:3] # Positions of particles at time t
        v = self.timesteps[t,:,3:6] # Velocities of particles at time t
        qr = np.dot(qvecs,r.T) # dot product of q and r vectros
        #normalize qvects         
        qvecs = (qvecs.T/qnorm).T
        qv = np.dot(qvecs,v.T) # dot product of q and v vectors
        #exp = np.exp(np.complex(0,1)*qr)
        # WHAT WE DO WITH THE COMPLEX NUMBER!!! Check from the gromax!
        exp = np.exp(np.complex(0,1)*qr) # Calculate exp part 

        # Longitudal component of particle current for each q vector
        Jq_L = np.sum(qv*exp,axis=1)
        # Calculate the cross product
        qxv = np.cross(qvecs[None],v[:,None])    

        # Output of qxv is a vector and we need norm of those vectors
        qxv=LA.norm(qxv,axis=2)
        
        # Indeces in qxv should be following [r]][q]
        # Transverse component of Particle current for each q vector
        #print("T:", qxv.shape)
        Jq_T = np.sum(qxv.T*exp,axis=1) 
       
        norm = np.sqrt(self.number_of_atoms)
        return Jq_L/norm, Jq_T/norm
        
    def particlecurrents(self,qvecs):
        """ (Trajectory,numpy array,int) -> (array, array, array)
        
        Calculates transverse and longitudal components of the 
        particle current for each frames in the system.
        Python version. Slow.
            
        Arguments:
        qvecs : Array of qvectors

        Returns:
        jq_L  : Longitudal part of particle current function
        jq_T  : Transverse part of particel current function 

        """
        
        jq_L = np.zeros((self.number_of_steps,len(qvecs)),dtype=np.complex128)
        jq_T = np.zeros((self.number_of_steps,len(qvecs)),dtype=np.complex128)

        for t in range(self.number_of_steps):
            print("Current step: %i /%i" %(t+1,self.number_of_steps))
            jl,jt = self.particlecurrent_frame(t,qvecs)
            jq_L[t,:] = jl
            jq_T[t,:] = jt
          
        return jq_L,jq_T


    def generate_qvecs(self,qmin=0.0,qmax=2.0):
        """ (self) -> array
        Generate an array of vectors in q-space using lattive vectors

        Arguments:
        qmin : minimum q value
        qmax : maximum q value

        Returns:
        qvecs : Array of q vectors 
        """
        
        qvecs = []
        # generate recoprocal space vectors
        self.r_vecs = np.zeros((3,3))        
        volume = (np.dot(self.latticev[0,:],np.cross(self.latticev[1,:],self.latticev[2,:])))
        self.r_vecs[0] =  (np.array(2*np.pi*np.cross(self.latticev[1,:],self.latticev[2,:])/volume))
        self.r_vecs[1] = (np.array(2*np.pi*np.cross(self.latticev[2,:],self.latticev[0,:])/volume))
        self.r_vecs[2] = (np.array(2*np.pi*np.cross(self.latticev[0,:],self.latticev[1,:])/volume))
        
        basics = np.array([[1,0,0],[0,1,0],[0,0,1]])

        D = 100
        for h in np.arange(-D,D+1.0,1.0):
            for j in np.arange(-D,D+1.0,1.0):
                for k in np.arange(-D,D+1.0,1.0):
                    if(h == j == k ==0.0):
                        pass
                    else:
                        qv = h*self.r_vecs[0]+j*self.r_vecs[1]+k*self.r_vecs[2]
                        qv_norm = np.sqrt(np.dot(qv,qv))
                        if qv_norm >= qmin and qv_norm < qmax:
                            qvecs.append(qv)

        # Sort qvectors asscending order
        qvecs = sorted(qvecs,key= lambda v: np.sqrt(np.dot(v,v)))        
        qvecs = np.array(qvecs)
        
        return qvecs                
        
    def currentcorrelation(self,qvecs,wsize=5000):
        """ (self) -> None
        
        Calculates longitudal and transverse current
        correlation functions from currents

        Arguments:
        qvecs : Q space vectros to calculate the correlation
        wsize : window size

        Returns:
        CL    : Longitudinal current correlation function 
        Jl    : Longitudinal particle current
        CT    : Transverse current correlation function
        Jt    : Transverse particle current
        qvecs : Q vectors which currents are calculated
        qset  : list of norms of q vectors 
        tmesh : Time mesh for correlation

        """


        # Order qvectors
        qvecs = sorted(qvecs,key= lambda v: np.sqrt(np.dot(v,v)))
        qvecs = np.array(qvecs)
        qnorm = LA.norm(qvecs,axis=1) # Lenghts of the qvectors
        qnorm = np.around(qnorm,5)
        qset = sorted(list(set(qnorm))) # Get ordered list of invidual norm values        

        nos = self.number_of_steps
        noa = self.number_of_atoms
        maxsteps = nos-wsize
        # get particle currents
        python = False
        if python:
            # This does not work proberly at the moment!
            Jl,Jt = self.particlecurrents(qvecs)
        else:
            Jl = np.zeros((self.number_of_steps,len(qvecs)),dtype=np.complex)
            Jt = np.zeros((self.number_of_steps,len(qvecs),3),dtype=np.complex)
            analyse.parcurrent(self.timesteps,Jl,Jt,qvecs,len(qvecs),noa,nos)

        CL = np.zeros((wsize,len(qset)))
        CT = np.zeros((wsize,len(qset)))
        tmesh = []

        # Correlation could be made with numpy.correlate
        python = False
        if python:
            for r in range(maxsteps):
                print("Current correlation step: %i/%i" %(r,maxsteps))
                for t in range(wsize):
                    jl_qvec = np.real(Jl[r+t]*Jl[r].conjugate())
                    jt_qvec = np.real(Jt[r+t]*Jt[r].conjugate())
                    #Collect correlation for each q-value
                    jl_corr = []
                    jt_corr = []
                    for s in qset:
                        ind = np.where(qnorm == s)
                        jl_corr.append(np.average(jl_qvec[ind]))
                        jt_corr.append(np.average(jt_qvec[ind]))                        
                        #jl_corr.append(np.sum(jl_qvec[ind]))
                        #jt_corr.append(np.sum(jt_qvec[ind]))
                    CL[t,:] = CL[t,:]+jl_corr
                    CT[t,:] = CT[t,:]+jt_corr
                #CL[t] = CL[t]+np.real(Jl[r+t]*Jl[r].conjugate())
                #CT[t] = CT[t]+np.real(Jt[r+t]*Jt[r].conjugate())
            CL = CL/float(maxsteps)
            CT = CT/(2.0*float(maxsteps))
        else:
            # Construct matrix for qset indeces and helper matrix
            qset_i =[]
            qset_n = []
            for s in qset:
                ind = list(np.where(qnorm == s)[0])
                qset_n.append(len(ind))
                qset_i.append(ind)
            # Find maximum number of q vector with same lenght
            qset_max = len(sorted(qset_i,key=len)[-1])
            # Make qset_i square array
            qset_i = np.array([xi+[-1]*(qset_max-len(xi)) for xi in qset_i]) 
            qset_n = np.array(qset_n)
            analyse.correlation(Jl,CL,qset_i,qset_n,wsize,maxsteps)
            analyse.correlation_vec(Jt,CT,qset_i,qset_n,wsize,maxsteps)
            CL = CL/float(maxsteps) # Average over all steps
            CT = 0.5*CT/(float(maxsteps)) # average over all steps
        for r in range(wsize):
            tmesh.append(r*0.02418884326505*125)            

        return CL,Jl,CT,Jt,qvecs,qset,tmesh
    
#        sname = "q0-2"
#        np.save("cl_qvecs."+sname+".npy",qvecs)
#        np.save("cl_data_l."+sname+".npy",CL)
#        np.save("cl_data_t."+sname+".npy",CT)
#        np.save("cl_data_jl."+sname+".npy",Jl)
#        np.save("cl_data_jt."+sname+".npy",Jt)
#        np.save("cl_qmesh."+sname+".npy",qset)
#        np.save("cl_tmesh."+sname+".npy",tmesh)


        
    def calculatecurrents(self,qvecs):
        """ (self) -> None
        
        Calculates longitudal and transverse currents and save them to disk

        Arguments:
        qvecs : List of Q vectors

        Returns:
        Jl : Longitudal particle current
        Jt : Transverse particle current
        
        NOTE: NEED FIXING! One should polish this function
        """

        
        # Order qvectors
        qvecs = sorted(qvecs,key= lambda v: np.sqrt(np.dot(v,v)))
        qvecs = np.array(qvecs)
        qnorm = LA.norm(qvecs,axis=1) # Lenghts of the qvectors
        qnorm = np.around(qnorm,5)
        #qset = sorted(list(set(qnorm))) # Get ordered list of invidual norm values


        nos = self.number_of_steps
        noa = self.number_of_atoms
        #maxsteps = nos//2
        # get particle currents
        python = False
        if python:
            Jl,Jt = self.particlecurrents(qvecs)
        else:
            Jl = np.zeros((self.number_of_steps,len(qvecs)),dtype=np.complex)
            Jt = np.zeros((self.number_of_steps,len(qvecs)),dtype=np.complex)
            analyse.parcurrent(self.timesteps,Jl,Jt,qvecs,len(qvecs),noa,nos)

        return Jl,Jt
        #print(qnorm)
        #print(CL[0])
        #print(CT[0])
        #np.save("cl_qvecs.100.npy",qvecs)
        #np.save("cl_data_jl.100.npy",Jl)
        #np.save("cl_data_jt.100.npy",Jt)
        #np.save("cl_nos.100.npy",nos)
        #np.save("cl_noa.100.npy",noa)


    def posvel_correlation_py(self):
      """ (Trajectory ) -> list
	
      Calculate velocity-position correlation. Python version.

      Arguments:
      None

      Returns:
      res   : list with velocity-position correlation
      rbins : list of bin centrals for vp-correlation

      """
      nos = self.number_of_steps
      noa = self.number_of_atoms
      dr = 0.2
      maxbin = 16.0
      rbins = np.arange(0.0,maxbin,dr)
      
      res = np.zeros((len(rbins)))
      for t in range(nos):
          print("Frame: "+str(t+1)+"/"+str(nos))
          rm = self.get_rangematrix_c(t)
          for i in range(noa):
              vi = self.timesteps[t][i][3:7]
              for j in range(noa):
                  if i == j:
                      continue
                  bindex = int(rm[i,j]//dr) # Index for rbin
                  if bindex > len(rbins)-1:
                      bindex = len(rbins)-1
                  vj  = self.timesteps[t][j][3:7]
                  res[bindex]= res[bindex] + np.dot(vi,vj)
	    #print("%i %i %i %e" %(i,j,bindex,rm[i,j]))
      # Normalize results
      #for i in range(len(res)):
      #    res[i] = res[i] #/float(nos)/float(noa)
      return res, rbins
 
    def posvel_correlation(self):
        """ (Trajectory ) -> list
        Calculate velocity-position correlation. C version.

        Arguments: 
        None
        
        Returns:
        res    : velocity-position correlation
        absres : absolute value of velocity-position correlation
        norm   : List of normalization coefficients

        NOTE: Need FIXING!
        """
        nos = self.number_of_steps
        noa = self.number_of_atoms
        dr = 0.2
        maxbin = 17.0
        rbins = np.arange(0.0,maxbin,dr)
      
        #res = [0.0 for x in range(len(rbins))]
        res = np.zeros((len(rbins)))
        absres = np.zeros((len(rbins)))
        norm = np.zeros((len(rbins)),dtype=np.int64)
      
        for t in range(nos):
            print("Frame: "+str(t+1)+"/"+str(nos))
            rm = self.get_rangematrix_c(t)
            analyse.velposcor(self.timesteps,rm,res,absres,norm,dr,t)
        # Normalize results
        for i in range(len(res)):
            if norm[i]== 0:
                nrm = 1.0
            else:
                nrm = float(norm[i])
	    #res[i] = res[i]/(float(nos)*float(nrm))
            res[i] = res[i]/(float(nrm))
            absres[i]=absres[i]/float(nrm)
        return res, absres,rbins,norm
 
 
    def vv_autocorr_atom(self,ni,maxstep=0):
        """ (Trajectory, int) -> list

        Calculate velocity velocity autocorrelation function
        for one atom 

        Arguments:
        ni : index of atom
        maxstep : maximum step to take account during calculation.

        Returns:
        results : v-v autocorrelation function

        """

        #Construct numpy array for autocorrelation function
        nos = self.number_of_steps

        result = np.zeros(maxstep)

        if maxstep == 0:
            maxstep = nos//2

        for k in range(maxstep):
            C = 0.0
            for i in range(nos-k):
                xi = self.timesteps[i][ni][3:7]
                xk = self.timesteps[i+k][ni][3:7]
                C = C +np.dot(xi,xk)/np.dot(xi,xi)
            C = C/(nos-k)
            result[k]=C
        return result

    def vv_autocorr_type_py(self,tpy=None,maxstep = 0):
        """ (Trajectory,list/str) -> list

        Calculates autocorrelation for one atomic type in the system
        if none is given calculate autocorrelation for all atoms
        Python version.
        
        Arguments:
        tpy     : list of atom index
        maxstep : Maximum timestep which is takeng account 
        """

        if tpy == None:
            tpys = self.atoms
        elif isinstance(tpy,str):
            tpys = [tpy]
        else:
            tpys = tpy

        # Find all atoms of tpy
        indeces = []
        for index in range(self.number_of_atoms):
            if self.atoms[index] in tpys:
                indeces.append(index)

        if maxstep == 0:
            maxstep = self.number_of_steps
            
        results = np.zeros(maxstep)
        indeces.sort()
        for ni in indeces:
            print(indeces.index(ni),"/", len(indeces))
            C =  self.vv_autocorr_atom(ni,maxstep)
            results = results + C

        results = results/float(len(indeces))
                
        return results

    def vv_autocorr_type(self,tpy=None,maxstep = 0,pp=False):
        """ (Trajectory,list/str,int,bool) -> list

        Calculates autocorrelation for one atomic type in the system
        if none is given calculate autocorrelation for all atoms
        C version.

        Arguments:
        tpy     : list of atom index
        maxstep : maximum step
        pp      : print progress        

        Returns:
        results : Auto correlation function for selected types
        """

        if tpy == None:
            tpys = self.atoms
        elif isinstance(tpy,str):
            tpys = [tpy]
        else:
            tpys = tpy

        # Find all atoms of tpy
        indeces = []
        for index in range(self.number_of_atoms):
            if self.atoms[index] in tpys:
                indeces.append(index)

        if maxstep == 0:
            maxstep = self.number_of_steps//2
            
        results = np.zeros(maxstep)
        C = np.zeros(maxstep)
        for ni in indeces:
            if pp:
                print(indeces.index(ni),"/", len(indeces))  
            analyse.autocorr_atom(self.timesteps,C,ni,self.number_of_atoms,maxstep,self.number_of_steps)
            results = results + C

        results = results/float(len(indeces))
                
        return results


    def diffusioncoef_vv(self,tpy=None):
        """ (self,list/str) -> (float)

        Calculate diffusion coefficient of given atomic type of if None is given
        calculate average diffusion coefficient. This one use velocity-velocity autocorrelation
        and Green-Kube -relation

        Arguments:
        tpy : list of atomic indeces

        Returns:
        None

        NOTE: EXPRIMENTAL NOT SURE HOW GOOD THE RESULTS ARE
        """

        if tpy == None:
            tpys = self.atoms
        elif isinstance(tpy,str):
            tpys = [tpy]
        else:
            tpys = tpy

        #Calculate autocorrelation function
        C = self.vv_autocorr_type(tpys)


        timestep = 125
        stepsize = timestep*0.02418884326505

        #Integrate autocorrelation function to obtain diffusion coefficient
        Csum = sum(C)
        print(Csum/(3.0*stepsize*len(C)))


    def diffusioncoef_dms(self,tpy=None):
        """ (Trajectory, list/styr) -> (float)

        Calculate diffusion coefficient of give atomic type or if None is given
        calculte the averatge diffusion coefficien. This one use Eisnstein relation.
        Unit of cm^2/s

        Arguments:
        tpy  : list of atomic indeces

        Returns:
        None

        NOTE: DOES NOT WORK! Need FIXING!
        """

        if tpy == None:
            tpys = self.atoms
        elif isinstance(tpy,str):
            tpys = [tpy]
        else:
            tpys = tpy

        R = self.squaremeandisplacement_type(tpys)
        
        timestep = 125
        stepsize = timestep*0.02418884326505

        #Calculate slope for the displacement        
        xrng = np.arange(len(R)*stepsize)
        print(xrng)
        

    def percolation_frame(self,t):
        """ (trajectory, int) -> bool 
        
        Check percollation from the trajectory

        Arguments:
        t : timestep
        
        Returns:
        percolate : True or false does trajectory percolates or not
    
        """
        rm = self.get_rm_percolation(t)
                
        # Get indeces of Au atoms for later use
        auindeces = [x for x in range(self.number_of_atoms) if self.atoms[t][x] == "Au"]
        # Set up list for which cluster each atom belong
        network = np.zeros((len(auindeces)),dtype=np.int)
        ncount = [0]        
        # define recursive function to check clusters
        def clustercheck(i,rngm):
            # First check if atom belong to network if not add new network
            ni = auindeces.index(i)
            if network[ni] == 0:
                ncount[0] += 1
                network[ni] = ncount[0]
            # Get current network
            curnet = network[ni]
            
            # Get neighnours of atom i
            nn, nnn = self.get_neighbours_atom(i,rngm)
            # if neighbours are crystal phase add them current network             
            for n in nn:
                if n in auindeces:
                    ni = auindeces.index(n)
                    # Check if neighbours are already in network if 
                    # not add them to the current network
                    # run clustercheck on them
                    if network[ni] == 0:
                        network[ni] = curnet
                        clustercheck(n,rngm)
                    # Recursion end when all neighbours and neighbours neighbours
                    # are added to current network
            pass # end of clustercheck function
            
        # Run clustercheck for all crystalline atoms
        for i in auindeces:
           clustercheck(i,rm)
        #
        # Construct list of clusters 
        nc = max(network) # get number of cluster
        clusters = [[] for x in range(nc)]
        for i in range(len(network)):
            clusters[network[i]-1].append(auindeces[i])
        
        rm_z = self.get_rm_percolation(t,True)

        def checkpercolation(c):
            
            for ai in c:
                nn, nnn = self.get_neighbours_atom(ai,rm) # with out PBC
                nn_z, nnn_z = self.get_neighbours_atom(ai,rm_z) # with PBC in z direction
                # Select those neighbours which are in cluster ie. connected with out PBCs
                au_nn = [n for n in nn if n in c]
                au_nn_z = [n for n in nn_z if n in c]
                
                if len(au_nn) != len(au_nn_z):
                    return True
                
            return False

        for c in clusters: 
            percolate = checkpercolation(c)
            if percolate:
                break

        return percolate


    def displacements_atom(self,ni,maxstep=0):
        """ (Trajectory, int,int) -> list

        Calculate displacements of one atom for trajectory.
        This should be calculate without moving the atoms back to original supercell
        Note: This is always calculated related to first position, not for previous
        frame.

        Arguments:
        ni      : atomi index
        maxstep : maximum step index to take account

        Returns:
        x  : list of displacement of atom ni
        """

        #Construct numpy array for displacements
        nos = self.number_of_steps

        x = np.zeros(nos)

        if maxstep == 0:
            maxstep = nos

        for i in range(maxstep):
            x[i] = np.sqrt(np.sum((self.timesteps[i][ni][0:3]-self.timesteps[0][ni][0:3])**2))
        
        return x

    
    def msd_atom(self,i):
        """ (self,int) -> (list)
        
        Calculate mean square displacement for one atom in the system

        Arguments:
        i : atomic index

        Returns:
        list :  list of square displacements for atom i
        """
        
        return self.displacements_atom(i)**2

    def msd_type(self,tpy=None):
        """ (Trajectory, list/str) -> (list)

        Calculates mean square displacement for one atomic type in the system
        if none is given calculate for all atoms

        Arguments:
        tpy : list of atomic indeces

        Results:
        results : mean square displacement for atoms of type tpy
        """

        if tpy == None:
            tpys = self.atoms
        elif isinstance(tpy,str):
            tpys = [tpy]
        else:
            tpys = tpy

        # Find all atoms of tpy
        indeces = []
        for index in range(self.number_of_atoms):
            if self.atoms[index] in tpys:
                indeces.append(index)

        nos = self.number_of_steps

        results = np.zeros(nos)
        
        for ni in indeces:
            results = results + self.displacements_atom(ni)**2
        results = results/float(len(indeces))
 
        return results

    def msd_atoms_output(self,index = None):
        """ (Trajectory, list/int) -> (none) 
        
        Calculate and output mean square displacement for invidual atoms
        in list given and save them to individual files
        
        Arguments:
        index : list of atomic indeces or index of atom

        Return:
        None
        
        """
        
        if index == None:
            index = range(self.number_of_atoms)
        if not isinstance(index, list):
            index = [index]            
            
        for i in index:
            ifn = "atoms_msd."+str(i)+".dat"
            ifile = open(ifn,"w")
            msds = self.msd_atom(i)
            for j in range(len(msds)):
                ifile.write("%i %f\n" %(j+1,msds[j]))
            ifile.close()

    def orderparam_atom(self,t,i,rm,sharp = True):
        """ (self,int,int,np.matrix,boolean) -> (np.array)
        
        Calculate sum vector from nearest neighbours (within SCUTOFF)
            
        Arguments:
        t     : timestep
        i     : atom to calculate
        rm    : range matrix for timestep t
        sharp : Do we use sharp cutoff

        Returns:
        vsum : Sum vector for atoms i
            
        """

        border = 0.5 # border after cutoff where 

        nn,nnn = self.get_neighbours_atom(i,rm)
        if sharp:
            cutoff = SCUTOFF
        else:
            cutoff = SCUTOFF + border
        shorts = [x for x in nn if rm[i,x]<cutoff] # Get only the short connections

        lnd = 11.512915464924779 # ln(d/(1-d)) calculated with 0.99999
        A = border/(2*lnd) # Scaling factor for fermi distribution
        c = SCUTOFF+border/2.0
        # Construc sum vector from short connections
        vsum = np.zeros(3)
        for n in shorts:
            ds = self.shortvec(t,i,n) # calculated vectro between atoms
            # Norm ds 
            dsnorm = np.sqrt(sum(ds**2))
            ds = ds/dsnorm
            if sharp:
                w = 1.0
            else:
                w = 1.0/(np.exp((rm[i,n]-c)/A)+1) # at the moment this is calculted wrong!??
#            print(w,dsnorm-SCUTOFF,np.exp((rm[i,n]-c)/A)+1)
            vsum =vsum+ds*w # we add weightened vectors to vectro sum ie. if it 
            #vsum =vsum+ds
        norm = np.sqrt(sum(vsum**2))
        if not norm == 0.0:
            vsum = vsum/norm
        else:
            vsum = np.zeros(3)
        return vsum


    def orderparam_frame_real(self,t):
        """ (self,int) -> (float)
        
        Calculate order parameters for atoms in frame and sort atoms 
        by order parameter. Print states of atoms as following in xyz format:
        Au - crystallize, Al - semi crystallize and Sb - amorphous
        Calculate also average orderparameter for frame and print 
        it as a comment line of xyz file.
           
        Arguments:
         t : timestep

        Returns:
        dotsum : List of order paramentes for atoms
        """

        rm = self.get_rangematrix_c(t)

        sumvecs = []
        arange = range(self.number_of_atoms)
        
        # Calculate atomic order parameter for all atoms
        svecs = []
        for ri in arange:
            ds = self.orderparam_atom(t,ri,rm,sharp=False)
            svecs.append(ds)
        
        for ri in arange:
            nn,nnn = self.get_neighbours_atom(ri,rm)
            # Find 6 closest neighbours
            nclosest = []
            for n in nn+nnn: # Go thought both nearest and next nearest neighbours
                nclosest.append([rm[ri,n],n])
            nclosest.sort()
            shorts = []

            for n in nclosest[0:6]:
                shorts.append(n[1]) # add index of nearest neighbours
            shorts.append(ri) # add atom it self to the shorts
            
            # Calculate sum of  nn sum vectors for 6 nearest neighbours
            vsum = np.zeros(3)
            for n in shorts:
                vsum =vsum+svecs[n]
            norm = np.sqrt(sum(vsum**2))
            if not norm == 0.0:
                vsum = vsum/norm
            else:
                vsum = np.zeros(3)
            sumvecs.append(vsum)

        # Calculate dot product between atom and its nearest neighbours
        dotsums = []
        for ri in arange:
            dotsum = 0.0
            if not len(nn) == 0:
                for n in nn:
                    dotsum += abs(np.dot(sumvecs[ri],sumvecs[n]))
                dotsum = dotsum/float(len(nn))
            else:
                dotsum = 0.5
            dotsums.append(dotsum)
       
        # Average results
        for ri in arange:  
            dsum = dotsums[ri]
            for n in nn:
                dsum += dotsums[n]
            dsum = dsum/float(len(nn)+1) 
            #print(ri,dotsums[ri],dsum)
            dotsums[ri] = dsum

        return dotsums

    def orderparam_frame(self,t):
        """ (self,int) -> (None)
        
        Calculate order parameters for atoms in frame and sort atoms 
        by order parameter. Print states of atoms as following in xyz format:
        Au - crystallize, Al - semi crystallize and Sb - amorphous
        Calculate also average orderparameter for frame and print 
        it as a comment line of xyz file.
           
        Arguments:
         t : timestep

        Returns:
         None
        """

        dotsums = self.orderparam_frame_real(t)

        # Print results in xyz format
        print("%i" %(self.number_of_atoms))
        #Calculate averga number for frame and print it
        avrg = sum(dotsums)/float(self.number_of_atoms)
        print("cstate: %f " %(avrg))
        
        arange = range(self.number_of_atoms)
        # Print results
        for ri in arange:
            #if dotsums[ri] < 0.51:
            if dotsums[ri] < 0.51:
                atom = "Sb"
            #elif dotsums[ri] < 0.60:
            elif dotsums[ri] < 0.60:                
                atom = "Al"
                #atom = "Sb"
            else:
                atom = "Au"
            c = self.timesteps[t][ri][0:3]
            # Debug print also dotsum
            print("%s    %f   %f   %f   %f"% (atom,c[0],c[1],c[2],dotsums[ri]))            
#            print("%s    %f   %f   %f"% (atom,c[0],c[1],c[2]))


    def orderparam_frame_c(self,t):
        """ (self,int) -> (None)
        
        Calculate order parameters for atoms in frame and sort atoms 
        by order parameter. Print states of atoms as following in xyz format:
        Au - crystallize, Al - semi crystallize and Sb - amorphous
        Calculate also average orderparameter for frame and print 
        it as a comment line of xyz file.
           
        Arguments
        t : timestep

        Returns:
        None
        """
        
        rm = self.get_rangematrix_c(t)
        tmatrix = np.matrix('0.0 0.0 0.0;0.0 0.0 0.0;0.0 0.0 0.0')
        tmatrix[:,:] = self.latticev.T
        
        inv_tmatrix = np.linalg.inv(tmatrix)
 
        dotsum = np.zeros(self.number_of_atoms)
        analyse.orderparam_frame(self.timesteps, inv_tmatrix,rm,dotsum,t,NNCUTOFF,NNNCUTOFF,0.5)

        print("%i" %(self.number_of_atoms))
        
        #Calculate averga number for frame and print it
        avrg = sum(dotsum)/float(self.number_of_atoms)
        print("cstate: %f " %(avrg))

        arange = range(self.number_of_atoms)
        # Print results
        for ri in arange:
            if dotsum[ri] < 0.51:
                atom = "Sb"
            elif dotsum[ri] < 0.60:
                atom = "Al"
            else:
                atom = "Au"
            c = self.timesteps[t][ri][0:3]
            print("%s    %f   %f   %f   %f"% (atom,c[0],c[1],c[2],dotsum[ri]))     


    def orderparam_field(self,t):
        """ (self,int) -> (None)
      
        Calculate nearest neighbour vectors for atoms and print
        atomic positions and vector in xyz format (+ 3 extra field)

        Arguments:
        t : timestep

        Returns:
        None

	"""
      
        rm = self.get_rangematrix_c(t)
    
        sumvecs = []
        arange = range(self.number_of_atoms)
        print(" %i " %(self.number_of_atoms))
        print("step:  %i" %(t+1))
        for ri in arange: 
            ds = self.orderparam_atom(t,ri,rm,sharp=False)
            c = self.timesteps[t][ri][0:3]
            print("%s %12.8f %12.8f %12.8f  %9.5f %9.5f %9.5f" % ("Sb",c[0],c[1],c[2],ds[0],ds[1],ds[2]))
      
           

    def orderparam_frame_old(self,t):
        """ (self,int) -> (None)
        
        Calculate order parameters for atoms in frame and sort atoms 
        by order parameter. Print states of atoms as following in xyz format:
        Au - crystallize, Al - semi crystallize and Sb - amorphous
        Calculate also average orderparameter for frame and print 
        it as a comment line of xyz file.
           
        Arguments:
        t : timestep

        Return:
        None
        """

        rm = self.get_rangematrix_c(t)
    
        sumvecs = []
        arange = range(self.number_of_atoms)
        for ri in arange: 
            
            
            nn,nnn = self.get_neighbours_atom(ri,rm)
            # Find 6 closest neighbours
            nclosest = []
            for n in nnn:
                nclosest.append([rm[ri,n],n])
            nclosest.sort()
            shorts = []
            for n in nclosest[0:6]:
                shorts.append(n[1])
            shorts.append(ri)
            # Calculate sum of  nn sum vectors for 6 nearest neighbours
            # !!! KOKEILE TÄTÄ VAIN NEAREST NEIGHBOUREILLE!!!!
            vsum = np.zeros(3)
            for n in shorts:
                ds = self.orderparam_atom(t,n,rm)
                vsum =vsum+ds
            norm = np.sqrt(sum(vsum**2))
            if not norm == 0.0:
                vsum = vsum/norm
            else:
                vsum = np.zeros(3)
            sumvecs.append(vsum)

        # Calculate dot product between atom and its nearest neighbours
        dotsums = []
        for ri in arange:
            nn,nnn = self.get_neighbours_atom(ri,rm)
            dotsum = 0.0
            if not len(nn) == 0:
                for n in nn:
            #print(np.dot(sumvecs[ri],sumvecs[n]))
                    dotsum += np.dot(sumvecs[ri],sumvecs[n])
                dotsum = dotsum/float(len(nn))
            else:
                dotsum = 0.5
            dotsums.append(dotsum)
       
        # Average results
        for ri in arange:
            nn,nnn = self.get_neighbours_atom(ri,rm)   
            dsum = dotsums[ri]
            for n in nn:
                dsum += dotsums[n]
            dsum = dsum/float(len(nn)+1) 
            #print(ri,dotsums[ri],dsum)
            dotsums[ri] = dsum

        # Print results in xyz format
        print("%i" %(self.number_of_atoms))
        #Calculate averga number for frame and print it
        avrg = sum(dotsums)/float(self.number_of_atoms)
        print("cstate: %f " %(avrg))        
        # Print results
        for ri in arange:
            if dotsums[ri] > -0.2:
                atom = "Sb"
            elif dotsums[ri] > -0.6:
                atom = "Al"
            else:
                atom = "Au"
            c = self.timesteps[t][ri][0:3]
            print("%s    %f   %f   %f"% (atom,c[0],c[1],c[2]))


    def ecn_frame(self,t):
        """
        Calculate effective coordination number for frame t
        (http://scitation.aip.org/content/aip/journal/jap/109/2/10.1063/1.3533422)
            
        Arguments:
        t : time frame
           
        Return:
        float : average effective coordinate number of frame t
        list  : effective coordination number for all atoms in frame t
        """        

        rm = self.get_rangematrix_c(t) # range matrix
        
        ecni = np.zeros(self.number_of_atoms)
        arange = range(self.number_of_atoms)

        for i in range(self.number_of_atoms):
            # Calculate dav 
            davready = False
            # select initial dav <
            dav = min(np.delete(rm[i,:],i))
            aran = arange[:]
            aran.remove(i)
            while not davready:
                sumdexp = 0.0
                sumexp = 0.0
                # Go through other atoms
                #exp = [np.exp(1-(rm[i,x]/dav)**6) for x in aran]
                #sumexp = sum(exp)
                #sumdexp = sum([rm[i,x]*exp[aran.index(x)] for x in aran])
                for j in aran:
                    exp =  np.exp(1-(rm[i,j]/dav)**6)
                    sumdexp += rm[i,j]*exp
                    sumexp += exp
                newdav = sumdexp/sumexp

                if np.abs(newdav-dav) < 0.00001:
                    davready = True
                dav = newdav
            # Dav is ready and now calculate effective coordination number
            #for j in aran:
            #    ecni[i] += np.exp(1-(rm[i,j]/dav)**6)
            #print("P: %i %f" %(i,dav))
            ecni[i] = sum([np.exp(1-(rm[i,j]/dav)**6) for j in aran])

        avrgecn = sum(ecni)/float(self.number_of_atoms)
        
        return avrgecn, ecni

    def ecn_py(self,pp=False):
        """
        Calculate average effective for trajectory, python version
        
        Arguments:
        pp : print progress

        Returns:
        aecn : average effective coordinate number
        """

        aecn = 0.0        
        for t in range(self.number_of_steps):
            if pp:
                print("%i / %i " %( t, self.number_of_steps))
            ecn,frame_ecns = self.ecn_frame(t)
            aecn +=ecn

        # return average ecn
        return aecn/float(self.number_of_steps)

    def ecn(self,pp=False):
        """
        Calculate average effective for trajectory using c routine.

        Arguments:
        pp : print progress. 

        Returns:
        aecn: Average effective coordinate number
        """

        aecn = 0.0        
        for t in range(self.number_of_steps):
            if pp:
                print("%i / %i " %( t, self.number_of_steps))
            rm = self.get_rangematrix_c(t) # range matrix
            ecni  = np.zeros(self.number_of_atoms)
            analyse.ecn_frame_c(rm,ecni,self.number_of_atoms)
            ecn = sum(ecni)/float(self.number_of_atoms) # Calculate average ecn for frame
            aecn +=ecn

        # return average effe
        return aecn/float(self.number_of_steps)

    def printframe_xyz(self,t,fh=None):
        """ (self,int,file) -> (str)        
        Print atomic positions of frame in XYZ format

        Arguments:
        t  : timestep
        fh : filehadler where to write the frame. If None output to stdout

        Returns:
        None
        """

        if fh == None:
            print(self.number_of_atoms)
            print(" STEP:  %i"% (t+1))
            for i in range(self.number_of_atoms):
                print ("%2s     %15.9f  %15.9f  %15.9f " % (self.atoms[i],
                    self.timesteps[t][i][0],self.timesteps[t][i][1],self.timesteps[t][i][2]))
                
        else:
            fh.write("%5i \n" %(self.number_of_atoms))
            fh.write(" STEP:  %i\n" % (t+1))
            for i in range(self.number_of_atoms):
                fh.write("%2s     %15.9f  %15.9f  %15.9f \n" % (self.atoms[i],
                    self.timesteps[t][i][0],self.timesteps[t][i][1],self.timesteps[t][i][2]))
            

            
    def printall_xyz(self,fn=None):
        """ (self,str) -> (str)     
        Print atomic positions of all frames in XYZ format

        Arguments:
        fn  : filename to wrtie the coordinates in xyz format.

        Returns:
        None
        """
        
        if fn == None:
            fh = None
        else:
            fh = open(fn,"w")

        for t in range(self.number_of_steps):
            self.printframe_xyz(t,fh)
        
        if not fh == None:
            fh.close()

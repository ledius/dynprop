# -*- coding: utf-8 -*-
#! /usr/bin/env python3

"""
Dynprop is a python package to analyze dynamica properties from
molecular dynamics calulation. 

Author: Matti Ropo, matti.ropo@tut.fi

"""


from  dynprop.trajectory import Trajectory

